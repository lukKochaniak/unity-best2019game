** Building game with implemented Leaderboard

In case you getting error like this:  
![picture](readme_screens/error.png)


Perform the following steps:  

1. Select option Window in Unity Editor  
![picture](readme_screens/1.jpg)  
2. From dropdown menu select General  
![picture](readme_screens/2.jpg)  
3. From next menu select "Test Runner"  
![picture](readme_screens/3.jpg)  
4. Window will appear. Click button with small icon of horizontal bars and arrow pointing down (top right corner of window)  
![picture](readme_screens/4.jpg)  
5. From dropdown menu select "Enable playmode tests for all assemblies".  
![picture](readme_screens/5.jpg)  
6. Restart Unity Editor.  