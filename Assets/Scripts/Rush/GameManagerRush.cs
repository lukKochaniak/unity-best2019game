﻿using Assets.Scripts.Common;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerRush : GameManager {
    #region SerializeFields
    [SerializeField]
    private Text playerOneTotalKilledLbl;
    [SerializeField]
    private Text playerTwoTotalKilledLbl;
    [SerializeField]
    private Text winnerText;
    [SerializeField]
    private GameObject WinnerUI;
    #endregion

    #region Singleton
    private static GameManagerRush instance = null;

    public static GameManagerRush Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<GameManagerRush>();
            } else if (instance != FindObjectOfType<GameManagerRush>()) {
                Destroy(FindObjectOfType<GameManagerRush>());
            }
            return instance;
        }
    }
    #endregion

    // Use this for initialization
    void Start() {
        WinnerUI.SetActive(false);
        CurrentWaveLbl.text = "Wave " + WaveNumber + " of " + TotalWaves;
        PlayBtn.gameObject.SetActive(false);
        showMenu();
    }

    void Update() {
        gameOverIfTowerDestroyed();
        joystickStartOrPSPressed();
        updateTotalKilledLabels();
    }

    private void updateTotalKilledLabels() {
        playerOneTotalKilledLbl.text = PlayerOneTotalKilled.ToString();
        playerTwoTotalKilledLbl.text = PlayerTwoTotalKilled.ToString();
    }

    private void gameOverIfTowerDestroyed() {
        foreach (Tower tower in TowerManager.Instance.PlayerOneTowers) {
            if (tower.CurrentHealth <= 0) {
                CurrentState = gameStatus.win; 
                showMenu();
                DestroyAllEnemies();
                showMenu();
            }
        }
        foreach (Tower tower in TowerManager.Instance.PlayerTwoTowers) {
            if (tower.CurrentHealth <= 0) {
                CurrentState = gameStatus.win;
                showMenu();
                DestroyAllEnemies();
                showMenu();
            }
        }
    }

    public override void showMenu() {
        switch (CurrentState) {
            case gameStatus.gameover:
                PlayBtnLbl.text = "Play Again!";
                break;
            case gameStatus.next:
                PlayBtnLbl.text = "Next Wave";
                break;
            case gameStatus.play:
                PlayBtnLbl.text = "Play";
                break;
            case gameStatus.win:
                setStats();
                foreach (Tower tower in TowerManager.Instance.PlayerOneTowers) {
                    if (tower.CurrentHealth <= 0)
                        winnerText.text = "Player 2";
                }
                foreach (Tower tower in TowerManager.Instance.PlayerTwoTowers) {
                    if (tower.CurrentHealth <= 0)
                        winnerText.text = "Player 1";
                }
                if (FirstTime) {
                    WinnerUI.SetActive(true);
                    FirstTime = false;
                }
                break;
        }
        PlayBtn.gameObject.SetActive(true);
        CurrentWaveLblObj.gameObject.SetActive(true);
    }

}
