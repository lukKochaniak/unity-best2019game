﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
    public GameObject PauseUI;
    [SerializeField]
    private GameObject[] buttons;
    private int currentButtonIndex = 0;
    private float changingCooldown = 0f;

    void Update() {
        joystickButtonsPressed();
        changeCooldown();
        highlightCurrentObject();
    }

    void changeCooldown() {
        changingCooldown -= Time.unscaledDeltaTime;
        if (changingCooldown < 0) {
            changingCooldown = 0;
        }
    }

    public void joystickButtonsPressed() {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetButtonDown("1PS4_Options") || Input.GetButtonDown("2PS4_Options")) {
            Toggle();
            highlightCurrentObject();
        }
        if (PauseUI.active) {
            if ((Input.GetAxis("1PS4_L_Vertical") > 0.5 || Input.GetAxis("2PS4_L_Vertical") > 0.5) && changingCooldown == 0) {
                moveDown();
                changingCooldown = 0.2f;
            }
            if ((Input.GetAxis("1PS4_L_Vertical") < -0.5 || Input.GetAxis("2PS4_L_Vertical") < -0.5) && changingCooldown == 0) {
                moveUp();
                changingCooldown = 0.2f;
            }
            if (Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X")) {
                determineActionOnSelectPressed();
            }
            if (Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O")) {
                Toggle();
            }
        }
    }

    private void highlightCurrentObject() {
        EventSystem.current.SetSelectedGameObject(buttons[currentButtonIndex]);
    }

    private void moveDown() {
        currentButtonIndex++;
        if (currentButtonIndex == buttons.Length)
            currentButtonIndex = 0;
    }

    private void moveUp() {
        currentButtonIndex--;
        if (currentButtonIndex < 0)
            currentButtonIndex = buttons.Length - 1;
    }

    private void determineActionOnSelectPressed() {
        string objectName = EventSystem.current.currentSelectedGameObject.name;
        if (objectName.Equals("Continue")) {
            Toggle();
        } else if (objectName.Equals("Retry")) {
            Retry();
        } else if (objectName.Equals("Menu")) {
            Menu();
            Time.timeScale = 1f;
        }
    }

    public void Toggle() {
        PauseUI.SetActive(!PauseUI.activeSelf);

        if (PauseUI.activeSelf) {
            Time.timeScale = 0f;
        } else {
            Time.timeScale = 1f;
        }
    }

    public void Retry() {
        Toggle();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Menu() {
        SceneManager.LoadScene("MainMenu");
    }
}
