﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour {

    [SerializeField]
    private GameObject[] levels;
    private int currentLevelIndex = 0;

    void Start() {
        EventSystem.current.SetSelectedGameObject(levels[currentLevelIndex]);
    }

    void Update() {
        joystickButtons();
    }

    private void joystickButtons() {
        if (Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X")) {
            PlayLevel();
        }
        if (Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O")) {
            MainMenu();
        }
        if (Input.GetButtonDown("1PS4_Square") || Input.GetButtonDown("2PS4_Square")) {
            GoToLeaderboard();
        }
    }

    public void PlayLevel() {
        string sceneName = EventSystem.current.currentSelectedGameObject.name;
        SceneManager.LoadScene(sceneName);
    }

    public void PlayLevel1() {
        SceneManager.LoadScene("PvpLevel1");
    }

    public void PlayLevel2() {
        SceneManager.LoadScene("PvpLevel2");
    }

    public void GoToLeaderboard() {
        SceneManager.LoadScene("Leaderboard");
    }

    public void MainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
