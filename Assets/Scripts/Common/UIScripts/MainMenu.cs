﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    [SerializeField]
    private GameObject[] buttons;
    [SerializeField]
    private GameObject[] labels;
    [SerializeField]
    private GameObject[] tutorials;
    private string currentButtonName;
    private int currentLabelIndex = 0;


    void Start() {
        EventSystem.current.SetSelectedGameObject(buttons[0]);
    }

    void Update() {
        joystickButtons();
        highlightInfo();
    }

    private void joystickButtons() {
        if ((Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X") || Input.GetKeyDown(KeyCode.X)) && !anyTutorialActive()) {
            playGameMode();
        }
        if ((Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X") || Input.GetKeyDown(KeyCode.X)) && anyTutorialActive()) {
            playScene();
        }
        if (Input.GetButtonDown("1PS4_Triangle") || Input.GetButtonDown("2PS4_Triangle") || Input.GetKeyDown(KeyCode.T)) {
            goToTutorial();
        }
        if (Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O") || Input.GetKeyDown(KeyCode.O)) {
            backToMenu();
        }
    }

    #region HighlightTutorial
    public void highlightInfo() {
        if (EventSystem.current.currentSelectedGameObject != null) {
            string buttonActiveName = EventSystem.current.currentSelectedGameObject.name;
            if (!anyTutorialActive()) {
                if (buttonActiveName.Equals("Co-op")) {
                    disableAllLabels("Co-op");
                    labels[0].SetActive(true);
                    currentLabelIndex = 0;
                } else if (buttonActiveName.Equals("PvpSelectLevel")) {
                    disableAllLabels("PvpSelectLevel");
                    labels[2].SetActive(true);
                    currentLabelIndex = 2;
                } else if (buttonActiveName.Equals("Rush")) {
                    disableAllLabels("Rush");
                    labels[1].SetActive(true);
                    currentLabelIndex = 1;
                } else if (buttonActiveName.Equals("Leaderboard")) {
                    disableAllLabels("Leaderboard");
                    labels[3].SetActive(true);
                    currentLabelIndex = 3;
                } else if (buttonActiveName.Equals("QuitButton")) {
                    disableAllLabels();
                }
            }
        }
        
    }

    private void disableAllLabels() {
        foreach (GameObject label in labels) {
            if(label != null)
                label.SetActive(false);
        }
    }

    private void disableAllLabels(string activeLabelName) {
        foreach (GameObject label in labels) {
            if (label != null && !label.name.Equals(activeLabelName)) {
                label.SetActive(false);
            }
        }
    }
    #endregion

    private void goToTutorial() {
        currentButtonName = EventSystem.current.currentSelectedGameObject.name;
        disableAllButtons();
        if (!EventSystem.current.currentSelectedGameObject.name.Equals("QuitButton")) {
            tutorials[currentLabelIndex].SetActive(true);
        }  
    }

    private void backToMenu() {
        tutorials[currentLabelIndex].SetActive(false);
        foreach (GameObject button in buttons) {
            if(button != null)
                button.SetActive(true);
        }
        foreach (GameObject label in labels) {
            if(label != null)
                label.SetActive(true);
        }
        currentLabelIndex = 0;
        EventSystem.current.SetSelectedGameObject(buttons[0]);

    }

    private bool anyTutorialActive() {
        bool isTutorialActive = false;
        foreach (GameObject tutorial in tutorials) {
            if (tutorial != null && tutorial.activeSelf) {
                isTutorialActive = true;
            }
        }
        return isTutorialActive;
    }

    private void disableAllButtons() {
        disableMenuButtons();
        disableAllLabels();
    }

    private void disableMenuButtons() {
        foreach (GameObject button in buttons) {
            if(button != null)
                button.SetActive(false);
        }
    }

    public void playScene() {
        SceneManager.LoadScene(currentButtonName);
    }

    public void playGameMode() {
        string sceneName = EventSystem.current.currentSelectedGameObject.name;
        if (sceneName.Equals("QuitButton")) {
            QuitGame();
        } else {
            SceneManager.LoadScene(sceneName);
        }
    }

    public void QuitGame() {
        Application.Quit();
    }
}
