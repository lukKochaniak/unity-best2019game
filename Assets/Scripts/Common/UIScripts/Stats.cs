﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    [SerializeField]
    private GameObject GameOverUi;
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O") || Input.GetKeyDown(KeyCode.O))) {
            this.gameObject.SetActive(false);
            GameOverUi.SetActive(true);
        }
    }
}
