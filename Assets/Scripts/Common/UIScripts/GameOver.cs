﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    [SerializeField]
    private GameObject[] buttons;
    [SerializeField]
    private GameObject stats;
    private int currentButtonIndex = 0;
    private float changingCooldown = 0f;
    private GameObject gameOver;

    public void Update() {
        joystickButtonsPressed();
        changeCooldown();
        highlightCurrentObject();
    }

    void changeCooldown() {
        changingCooldown -= Time.unscaledDeltaTime;
        if (changingCooldown < 0) {
            changingCooldown = 0;
        }
    }

    private void highlightCurrentObject() {
        EventSystem.current.SetSelectedGameObject(buttons[currentButtonIndex]);
    }

    private void moveDown() {
        currentButtonIndex++;
        if (currentButtonIndex == buttons.Length)
            currentButtonIndex = 0;
    }

    private void moveUp() {
        currentButtonIndex--;
        if (currentButtonIndex < 0)
            currentButtonIndex = buttons.Length - 1;
    }

    private void joystickButtonsPressed() {
        if (!stats.activeSelf) {
            if ((Input.GetAxis("1PS4_L_Vertical") > 0.5 || Input.GetAxis("2PS4_L_Vertical") > 0.5 || Input.GetKeyDown(KeyCode.J)) && changingCooldown == 0) {
                moveDown();
                changingCooldown = 0.2f;
            }
            if ((Input.GetAxis("1PS4_L_Vertical") < -0.5 || Input.GetAxis("2PS4_L_Vertical") < -0.5 || Input.GetKeyDown(KeyCode.U)) && changingCooldown == 0) {
                moveUp();
                changingCooldown = 0.2f;
            }
            if (Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X") || Input.GetKeyDown(KeyCode.X)) {
                determineActionOnSelectPressed();
            }
            if (Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O") || Input.GetKeyDown(KeyCode.O)) {
                Menu();
            }
        }

    }

    private void determineActionOnSelectPressed() {
        if (currentButtonIndex == 1) {
            showStatistics();
        } else if (currentButtonIndex == 0) {
            Retry();
        } else if (currentButtonIndex == 2) {
            Menu();
        }
    }

    private void showStatistics() {
        gameOver = this.gameObject;
        gameOver.SetActive(false);
        stats.SetActive(true);
    }

    public void Retry() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Menu() {
        SceneManager.LoadScene("MainMenu");
    }

}
