﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class AboutModeMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject[] buttons;
    private int currentButtonIndex = 0;

    void Start()
    {
        EventSystem.current.SetSelectedGameObject(buttons[currentButtonIndex]);
    }

    void Update()
    {
        joystickButtons();
    }

    private void joystickButtons()
    {
        if (Input.GetButtonDown("1PS4_X") || Input.GetButtonDown("2PS4_X"))
        {
            playGameMode();
        }
    }

    public void playGameMode()
    {
        string sceneName = EventSystem.current.currentSelectedGameObject.name;
        if (sceneName.Equals("QuitButton"))
        {
            BackToMainMenu();
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
    }

    public void PlayGameCoop()
    {
        SceneManager.LoadScene("Co-op");
    }

    public void PlayGameRush()
    {
        SceneManager.LoadScene("Rush");
    }

    public void PlayGamePvp()
    {
        SceneManager.LoadScene("PvpSelectLevel");
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
