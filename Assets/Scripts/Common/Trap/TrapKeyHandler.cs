﻿using UnityEngine;
using UnityEngine.UI;

public class TrapKeyHandler : MonoBehaviour {

    #region Fields
    private TrapBtn btnP1 = new TrapBtn();
    private TrapBtn btnP2 = new TrapBtn();

    public Sprite dragSpriteLava;
    public Sprite dragSpriteMud;

    public Trap lavaTr;
    public Trap mudTr;
    #endregion

    void Update() {
        if (Input.GetButtonDown("1PS4_Triangle") && !TrapManager.Instance.P1settingTrap) {

            if (TrapManager.Instance.GameManager.PlayerOneMoney >= TrapManager.Instance.TrapPrice) {
                TrapManager.Instance.P1settingTrap = true;
                btnP1.trapObject = lavaTr;
                btnP1.trapObject.type = trapType.lavaTrap;
                btnP1.dragSprite = dragSpriteLava;
                TrapManager.Instance.P1settingTrap = true;
                StartTrapSetting(btnP1, true, true);
            }

        } else if (Input.GetButtonDown("1PS4_Square") && !TrapManager.Instance.P1settingTrap) {

            if (TrapManager.Instance.GameManager.PlayerOneMoney >= TrapManager.Instance.TrapPrice) {
                TrapManager.Instance.P1settingTrap = true;
                btnP1.trapObject = mudTr;
                btnP1.trapObject.type = trapType.mudTrap;
                btnP1.dragSprite = dragSpriteMud;
                TrapManager.Instance.P1settingTrap = true;
                StartTrapSetting(btnP1, true, false);
            }

        } else if(Input.GetButtonDown("1PS4_Triangle") && TrapManager.Instance.P1settingTrap) { //if P1 wants to change trap that they're setting

            btnP1.trapObject = lavaTr;
            btnP1.trapObject.type = trapType.lavaTrap;
            btnP1.dragSprite = dragSpriteLava;
            UpdateTrapSetting(btnP1, true, true);

        } else if (Input.GetButtonDown("1PS4_Square") && TrapManager.Instance.P1settingTrap) {

            btnP1.trapObject = mudTr;
            btnP1.trapObject.type = trapType.mudTrap;
            btnP1.dragSprite = dragSpriteMud;
            UpdateTrapSetting(btnP1, true, false);

        } else if (Input.GetButtonDown("2PS4_Triangle") && !TrapManager.Instance.P2settingTrap) {

            if (TrapManager.Instance.GameManager.PlayerTwoMoney >= TrapManager.Instance.TrapPrice) {
                TrapManager.Instance.P2settingTrap = true;
                btnP2.trapObject = lavaTr;
                btnP2.trapObject.type = trapType.lavaTrap;
                btnP2.dragSprite = dragSpriteLava;
                TrapManager.Instance.P2settingTrap = true;
                StartTrapSetting(btnP2, false, true);
            }

        } else if (Input.GetButtonDown("2PS4_Square") && !TrapManager.Instance.P2settingTrap) {

            if (TrapManager.Instance.GameManager.PlayerTwoMoney >= TrapManager.Instance.TrapPrice) {
                TrapManager.Instance.P2settingTrap = true;
                btnP2.trapObject = mudTr;
                btnP2.trapObject.type = trapType.mudTrap;
                btnP2.dragSprite = dragSpriteMud;
                TrapManager.Instance.P2settingTrap = true;
                StartTrapSetting(btnP2, false, false);
            }

        } else if (Input.GetButtonDown("2PS4_Triangle") && TrapManager.Instance.P2settingTrap) { //if P2 wants to change trap that they're setting

            btnP2.trapObject = lavaTr;
            btnP2.trapObject.type = trapType.lavaTrap;
            btnP2.dragSprite = dragSpriteLava;
            UpdateTrapSetting(btnP2, false, true);

        }
        else if (Input.GetButtonDown("2PS4_Square") && TrapManager.Instance.P2settingTrap) {

            btnP2.trapObject = mudTr;
            btnP2.trapObject.type = trapType.mudTrap;
            btnP2.dragSprite = dragSpriteMud;
            UpdateTrapSetting(btnP2, false, false);

        }


    }

    private void StartTrapSetting(TrapBtn btnSelected, bool isPlayerOne, bool isLavaTrap)
    {
        TrapSetter setter = TrapManager.Instance.TrapSetters[0];
        TrapSetter setter2 = TrapManager.Instance.TrapSetters[1];

        if (setter.IsPlayerOne == true && isPlayerOne)
        {
            setter.SelectedTrap(btnSelected, isLavaTrap);
        }
        else if(setter.IsPlayerOne == false && !isPlayerOne)
        {
            setter.SelectedTrap(btnSelected, isLavaTrap);
        }
        else if(setter2.IsPlayerOne && isPlayerOne)
        {
            setter2.SelectedTrap(btnSelected, isLavaTrap);
        }
        else if(setter2.IsPlayerOne == false && !isPlayerOne)
        {
            setter2.SelectedTrap(btnSelected, isLavaTrap);
        }
    }

    private void UpdateTrapSetting(TrapBtn btnSelected, bool isPlayerOne, bool isLavaTrap)
    {
        TrapSetter setter = TrapManager.Instance.TrapSetters[0];
        TrapSetter setter2 = TrapManager.Instance.TrapSetters[1];

        if (setter.IsPlayerOne == true && isPlayerOne)
        {
            setter.UpdateTrap(btnSelected, isLavaTrap);
        }
        else if (setter.IsPlayerOne == false && !isPlayerOne)
        {
            setter.UpdateTrap(btnSelected, isLavaTrap);
        }
        else if (setter2.IsPlayerOne && isPlayerOne)
        {
            setter2.UpdateTrap(btnSelected, isLavaTrap);
        }
        else if (setter2.IsPlayerOne == false && !isPlayerOne)
        {
            setter2.UpdateTrap(btnSelected, isLavaTrap);
        }
    }

}

