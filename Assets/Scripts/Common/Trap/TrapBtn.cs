﻿using UnityEngine;

public class TrapBtn : MonoBehaviour {
    public Trap trapObject;
    public Sprite dragSprite;

    //public GameObject TrapObject
    public Trap TrapObject {
        get { return trapObject; }
    }

    public Sprite DragSprite {
        get { return dragSprite; }
    }
}
