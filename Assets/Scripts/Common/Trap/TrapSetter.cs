﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSetter : MonoBehaviour {

    #region Fields
    [SerializeField]
    private bool isPlayerOne;

    private TrapBtn trapBtnPressed;
    private SpriteRenderer spriteRenderer;
    private bool isLavaTrap;

    [SerializeField]
    private Sprite lavaTrapCancelSprite;

    [SerializeField]
    private Sprite mudTrapCancelSprite;

    [SerializeField]
    private Sprite lavaTrapSetSprite;

    [SerializeField]
    private Sprite mudTrapSetSprite;

    private TrapManager trapManager;
    #endregion

    public bool IsPlayerOne
    {
        get { return isPlayerOne; }
    }

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        trapManager = TrapManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("1PS4_X") && trapManager.P1settingTrap && this.isPlayerOne)
        {
            foreach (GameObject obj in trapManager.Objects)
            {
                if (Vector3.Distance(obj.transform.position, transform.position) < 0.5)
                {
                    trapBtnPressed.TrapObject.transform.position = obj.transform.position;
                    Trap newTrap = Instantiate(trapBtnPressed.TrapObject) as Trap;
                    newTrap.IsFromPlayerOne = true;

                    if (newTrap.tag == "MudTrap")
                    {
                        newTrap.Type = trapType.mudTrap;
                    }
                    else if (newTrap.tag == "LavaTrap")
                    {
                        newTrap.Type = trapType.lavaTrap;
                    }

                    trapManager.AddTrap(newTrap);

                    newTrap.transform.position = obj.transform.position;
                    DisableDragSprite();
                    trapManager.PayForTrap(true);
                    trapManager.P1settingTrap = false;

                    return;
                }
            }

        }
        else if (Input.GetButtonDown("2PS4_X") && TrapManager.Instance.P2settingTrap && (this.isPlayerOne == false))
        {
            foreach (GameObject obj in TrapManager.Instance.Objects)
            {
                if (Vector3.Distance(obj.transform.position, transform.position) < 0.5)
                {
                    trapBtnPressed.TrapObject.transform.position = 
                        obj.transform.position;
                    Trap newTrap = Instantiate(trapBtnPressed.TrapObject) as Trap;
                    newTrap.IsFromPlayerOne = false;

                    if (newTrap.tag == "MudTrap")
                    {
                        newTrap.Type = trapType.mudTrap;
                    }
                    else if (newTrap.tag == "LavaTrap")
                    {
                        newTrap.Type = trapType.lavaTrap;
                    }

                    trapManager.AddTrap(newTrap);

                    DisableDragSprite();
                    trapManager.PayForTrap(false);
                    trapManager.P2settingTrap = false;

                    return;
                }
            }
        }

        if (spriteRenderer.enabled)
        {
            CheckIfToCancelSetting();
            MoveTrap();
        }
    }

    private void MoveTrap()
    {
        float speed = 3.0f;
        if (trapManager.P1settingTrap && isPlayerOne)
        {
            if (Input.GetKey(KeyCode.A) || Input.GetAxis("1PS4_D_X") < -0.1)
            {
                transform.position += Vector3.left * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.D) || Input.GetAxis("1PS4_D_X") > 0.1)
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.W) || Input.GetAxis("1PS4_D_Y") > 0.1)
            {
                transform.position += Vector3.up * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.S) || Input.GetAxis("1PS4_D_Y") < -0.1)
            {
                transform.position += Vector3.down * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
        }
        else if (trapManager.P2settingTrap && !isPlayerOne)
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxis("2PS4_D_X") < -0.1)
            {
                transform.position += Vector3.left * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxis("2PS4_D_X") > 0.1)
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetAxis("2PS4_D_Y") > 0.1)
            {
                transform.position += Vector3.up * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetAxis("2PS4_D_Y") < -0.1)
            {
                transform.position += Vector3.down * speed * Time.deltaTime;
                UpdateSpriteByPosition();
            }
        }
    }

    private void CheckIfToCancelSetting()
    {
        if (trapManager.P1settingTrap && isPlayerOne)
        {
            if (Input.GetButtonDown("1PS4_O"))
            {
                spriteRenderer.sprite = null;
                trapManager.P1settingTrap = false;
                DisableDragSprite();
            }
        }
        else if (trapManager.P2settingTrap && !isPlayerOne)
        {
            if(Input.GetButtonDown("2PS4_O"))
            {
                spriteRenderer.sprite = null;
                trapManager.P2settingTrap = false;
                DisableDragSprite();
            }
        }
    }

    public void SelectedTrap(TrapBtn trapSelected, bool isLavaTrap)
    {
        trapBtnPressed = trapSelected;
        this.isLavaTrap = isLavaTrap;
        EnableDragSprite(trapBtnPressed.DragSprite);
    }

    public void UpdateTrap(TrapBtn trapUpdate, bool isLavaTrap)
    {
        trapBtnPressed = trapUpdate;
        this.isLavaTrap = isLavaTrap;
        spriteRenderer.sprite = trapBtnPressed.DragSprite;
        UpdateSpriteByPosition();
    }

    public void EnableDragSprite(Sprite sprite)
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = true;
        spriteRenderer.sprite = sprite;
        transform.position = new Vector2(-0.1f, -1.5f);

        if (isLavaTrap)
        {
            spriteRenderer.sprite = lavaTrapCancelSprite;
        }
        else
        {
            spriteRenderer.sprite = mudTrapCancelSprite;
        }
    }

    public void DisableDragSprite()
    {
        spriteRenderer.enabled = false;
    }

    private void UpdateSpriteByPosition()
    {
        foreach (GameObject obj in TrapManager.Instance.Objects)
        {
            if (Vector3.Distance(obj.transform.position, transform.position) < 0.5)
            {
                if(isLavaTrap)
                {
                    spriteRenderer.sprite = lavaTrapSetSprite;
                }
                else
                {
                    spriteRenderer.sprite = mudTrapSetSprite;
                }

                return;
            }

            if (isLavaTrap)
            {
                spriteRenderer.sprite = lavaTrapCancelSprite;
            }
            else
            {
                spriteRenderer.sprite = mudTrapCancelSprite;
            }
        }
    }
}
