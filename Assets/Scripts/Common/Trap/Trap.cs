﻿using UnityEngine;

public enum trapType {
    lavaTrap, mudTrap
};

public class Trap : MonoBehaviour {

    [SerializeField]
    public trapType type;

    private int hitCounter = 0;

    private bool isFromPlayerOne;

    #region GettersAndSetters
    public trapType Type {
        get { return type; }
        set { type = value; }
    }

    public bool IsFromPlayerOne {
        get { return isFromPlayerOne; }
        set { isFromPlayerOne = value; }
    }
    #endregion

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Enemy") {
            hitCounter++;

            if (hitCounter >= 3) {
                Destroy(this.gameObject);
            }
        }
    }
}
