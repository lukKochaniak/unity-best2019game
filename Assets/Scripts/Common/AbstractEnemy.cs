﻿using Assets.Scripts.Common;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Common {
    public abstract class AbstractEnemy : MonoBehaviour {

        #region SerializeFields
        [SerializeField]
        private Transform[] waypoints;
        [SerializeField]
        private float navigationUpdate;
        [SerializeField]
        private float healthPoints;
        [SerializeField]
        private int rewardAmonut;
        [SerializeField]
        private Transform healthBar;
        [SerializeField]
        private Slider healthBarFill;
        [SerializeField]
        private float healthBarFillOffset = -0.8f;
        #endregion

        #region Fields
        private GameManager gameManager;
        private bool facingRight = true;
        private int curveNumber;
        private bool movingForward = true;
        private int startIndex;
        private Transform[] currentWaypoints;
        private int target = 0;
        private Transform enemy;
        private Collider2D enemyCollider;
        private Animator animator;
        private float navigationTime = 0;
        private bool isDead = false;
        private bool enabledWaypoints = false;
        private float currentHealth;
        private float speed = 0.8f;
        private bool isTargettingTower = false;
        #endregion

        #region GettersAndSetter
        public bool IsDead {
            get { return isDead; }
        }

        public int StartIndex {
            get { return startIndex; }
            set { startIndex = value; }
        }

        public GameManager GameManager {
            get { return gameManager; }
            set { gameManager = value; }
        }
        #endregion

        /*
        // Use this for initialization
        void Start() {
            enemy = GetComponent<Transform>();
            enemyCollider = GetComponent<Collider2D>();
            animator = GetComponent<Animator>();
            registerEnemyDependingOnGameMode();
            currentHealth = healthPoints;
        }

        // Update is called once per frame
        void Update() {
            if (waypoints != null && !isDead) {
                navigationTime += Time.deltaTime;
                Vector3 oldPosition = transform.position;
                if (navigationTime > navigationUpdate) {
                    if (enabledWaypoints) {
                        try {
                            enemy.position = Vector2.MoveTowards(enemy.position, currentWaypoints[target].position, speed * navigationTime);
                        } catch { }
                        if (SceneManager.GetActiveScene().name.Equals("Rush") && oldPosition == transform.position) {
                            findTarget(); //getting enemies unstuck in Rush mode
                        }
                    } else {
                        enemy.position = Vector2.MoveTowards(enemy.position, curvesStart[startIndex].position, speed * navigationTime);
                    }
                    navigationTime = 0;
                    bool movingRight;
                    if (transform.position.x >= oldPosition.x)
                        movingRight = true;
                    else
                        movingRight = false;
                    faceMovementDirection(movingRight);
                }
            }
            PostionHealthBar();
        }


        private void registerEnemyDependingOnGameMode() {
            gameManager.RegisterEnemy(this);
        }

        private void faceMovementDirection(bool movingRight) {
            if (movingRight && !facingRight) {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                facingRight = true;
            } else if (!movingRight && facingRight) {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                facingRight = false;
            }
        }

        private void findTarget() {
            if (movingForward)
                target++;
            else {
                if (target > 0)
                    target--;
                else {
                    if (curveNumber == 0) {
                        if (targetTower == possibleTowerTargets[0])
                            currentWaypoints = playerOneFirstCurve;
                        if (targetTower == possibleTowerTargets[1])
                            currentWaypoints = playerTwoFirstCurve;
                        target = 1;
                    } else if (curveNumber == 1) {
                        if (targetTower == possibleTowerTargets[0])
                            currentWaypoints = playerOneSecondCurve;
                        if (targetTower == possibleTowerTargets[1])
                            currentWaypoints = playerTwoSecondCurve;
                        target = 1;
                    } else if (curveNumber == 2) {
                        if (targetTower == possibleTowerTargets[0])
                            currentWaypoints = playerOneThirdCurve;
                        if (targetTower == possibleTowerTargets[1])
                            currentWaypoints = playerTwoThirdCurve;
                        target = 1;
                    } else if (curveNumber == 0) {
                        if (targetTower == possibleTowerTargets[0])
                            currentWaypoints = playerOneFourthCurve;
                        if (targetTower == possibleTowerTargets[1])
                            currentWaypoints = playerTwoFourthCurve;
                        target = 1;
                    }

                }
            }
        }

        private void PostionHealthBar() {
            healthBarFill.value = currentHealth / healthPoints;
            Vector3 currentPos = transform.position;

            healthBar.position = new Vector3(currentPos.x, currentPos.y + healthBarFillOffset, currentPos.z);
            //healthBar.LookAt(Camera.main.transform);
        }

        void OnTriggerEnter2D(Collider2D other) {
            if (other.tag == "Checkpoint") {
                if (movingForward)
                    target++;
                else
                    target--;
            } else if (other.tag == "Finish") {
                determineFinishResultDependingOnGameMode();
            }

            if (other.tag == "PlayerOneTower" && SceneManager.GetActiveScene().name.Equals("Rush")) {
                gameManager.substractMonet(rewardAmonut * 2, true);
                enemyEscapedRoutine();
                other.gameObject.GetComponent<Tower>().CurrentHealth--;
            } else if (other.tag == "PlayerTwoTower" && SceneManager.GetActiveScene().name.Equals("Rush")) {
                gameManager.substractMonet(rewardAmonut * 2, false);
                enemyEscapedRoutine();
                other.gameObject.GetComponent<Tower>().CurrentHealth--;
            } else if (other.tag == "PlayerOneFinish") {
                gameManager.substractMonet(rewardAmonut * 2, true);
                enemyEscapedRoutine();
            } else if (other.tag == "PlayerTwoFinish") {
                gameManager.substractMonet(rewardAmonut * 2, false);
                enemyEscapedRoutine();
            }

            if (other.tag == "Projectile") {
                Projectile newP = other.gameObject.GetComponent<Projectile>();
                enemyHit(newP.AttackStrength, newP.IsFromPlayerOne);
                Destroy(other.gameObject);
            } else if (other.tag == "CurveOne" || other.tag == "CurveTwo" || other.tag == "CurveThree" || other.tag == "CurveFour") {
                waypointsDetermination(other.tag);
            } else if (other.tag == "MudTrap") {
                slowFromTrap();
            } else if (other.tag == "LavaTrap") {
                //enemyHit(100, true);
                getDamageFromTrap();
            }
        }

        private void enemyEscapedRoutine() {
            gameManager.RoundEscaped++;
            gameManager.TotalEscaped++;
            gameManager.UnregisterEnemy(this);
            gameManager.isWaveOver();
        }

        private void getDamageFromTrap() {
            float damage = 0.2f * currentHealth;
            currentHealth -= damage;
        }

        private void slowFromTrap() {
            speed /= 2;
        }

        private void determineFinishResultDependingOnGameMode() {
            if (SceneManager.GetActiveScene().name.Equals("Co-op")) {
                GameManagerCoop.Instance.Health--;
            }
            enemyEscapedRoutine();
        }

        private void waypointsDetermination(string tagName) {
            enabledWaypoints = true;
            //int index = Random.Range(0, 2);
            if (!isTargettingTower || !SceneManager.GetActiveScene().name.Equals("Rush")) {
                if (tagName == "CurveOne") {
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerOneTowers[0]); //possible targets and targetTower only for Rush
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerTwoTowers[0]);
                    int i = setRandomlyCurrentWaypoints(playerOneFirstCurve, playerTwoFirstCurve);
                    targetTower = possibleTowerTargets[i];
                    curveNumber = 0;
                } else if (tagName == "CurveTwo") {
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerOneTowers[1]);
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerTwoTowers[1]);
                    int i = setRandomlyCurrentWaypoints(playerOneSecondCurve, playerTwoSecondCurve);
                    targetTower = possibleTowerTargets[i];
                    curveNumber = 1;
                } else if (tagName == "CurveThree") {
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerOneTowers[2]);
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerTwoTowers[2]);
                    int i = setRandomlyCurrentWaypoints(playerOneThirdCurve, playerTwoThirdCurve);
                    targetTower = possibleTowerTargets[i];
                    curveNumber = 2;
                } else if (tagName == "CurveFour") {
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerOneTowers[3]);
                    possibleTowerTargets.Add(TowerManager.Instance.PlayerTwoTowers[3]);
                    int i = setRandomlyCurrentWaypoints(playerOneFourthCurve, playerTwoFourthCurve);
                    targetTower = possibleTowerTargets[i];
                    curveNumber = 3;
                }
                isTargettingTower = true;
            } else {
                if (tagName == "CurveOne") {
                    if (targetTower == possibleTowerTargets[0])
                        currentWaypoints = playerOneFirstCurve;
                    else
                        currentWaypoints = playerTwoFirstCurve;
                } else if (tagName == "CurveTwo") {
                    if (targetTower == possibleTowerTargets[0])
                        currentWaypoints = playerOneSecondCurve;
                    else
                        currentWaypoints = playerTwoSecondCurve;
                } else if (tagName == "CurveThree") {
                    if (targetTower == possibleTowerTargets[0])
                        currentWaypoints = playerOneThirdCurve;
                    else
                        currentWaypoints = playerTwoThirdCurve;
                } else if (tagName == "CurveFour") {
                    if (targetTower == possibleTowerTargets[0])
                        currentWaypoints = playerOneFourthCurve;
                    else
                        currentWaypoints = playerTwoFourthCurve;
                }
                target = 1;
                movingForward = true;
            }
        }

        private int setRandomlyCurrentWaypoints(Transform[] playerOneWaypoints, Transform[] playerTwoWaypoints) {
            int index = Random.Range(0, 2);
            if (SceneManager.GetActiveScene().name.Equals("Rush"))
                target = 1;
            else
                target = 0;
            if (index == 0) {
                currentWaypoints = playerOneWaypoints;
            } else {
                currentWaypoints = playerTwoWaypoints;
            }
            return index;
        }

        public void enemyHit(int hitpoints, bool isFromPlayerOne) {
            if (SceneManager.GetActiveScene().name.Substring(0, 3).Equals("Pvp")) {
                GameManagerPvp.Instance.DamageHandler(hitpoints, isFromPlayerOne);
            }
            if (SceneManager.GetActiveScene().name.Equals("Rush")) {
                if (isFromPlayerOne && targetTower == possibleTowerTargets[0]) {
                    targetTower = possibleTowerTargets[1];
                    movingForward = false;
                    target--;
                } else if (!isFromPlayerOne && targetTower == possibleTowerTargets[1]) {
                    targetTower = possibleTowerTargets[0];
                    movingForward = false;
                    target--;
                }
            }
            if (currentHealth - hitpoints > 0) {
                currentHealth -= hitpoints;
                animator.Play("OneHurt");
            } else {
                currentHealth = 0;
                die(isFromPlayerOne);
            }
            determineAccuracy(hitpoints, isFromPlayerOne);

        }

        private void determineAccuracy(int strength, bool isPlayerOne) {
            if (isPlayerOne) {
                gameManager.TotalPlayerOneHits++;
                gameManager.TotalPlayerOneDamage += strength;
            } else {
                gameManager.TotalPlayerTwoHits++;
                gameManager.TotalPlayerTwoDamage += strength;
            }
        }

        public void die(bool isFromPlayerOne) {
            isDead = true;
            FindObjectOfType<AudioManager>().PlaySound("EnemyDieSong");
            animator.SetTrigger("didDie");
            enemyCollider.enabled = false;
            determineDieResultDependingOnGameMode(isFromPlayerOne);
        }

        private void determineDieResultDependingOnGameMode(bool isFromPlayerOne) {
            gameManager.TotalKilled++;
            gameManager.addMoney(rewardAmonut, isFromPlayerOne);
            gameManager.isWaveOver();
            if (isFromPlayerOne) {
                gameManager.PlayerOneTotalKilled++;
            } else {
                gameManager.PlayerTwoTotalKilled++;
            }
        }*/

    }
}

