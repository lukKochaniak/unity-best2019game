﻿using Assets.Scripts.Common;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class TrapManager : Singleton<TrapManager> {

    #region Fields
    [SerializeField]
    private GameManager gameManager;

    private GameObject[] objects;
    private TrapSetter[] trapSetters;

    private int trapPrice = 5;

    public bool P1settingTrap;
    public bool P2settingTrap;

    private List<Trap> trapList = new List<Trap>();
    #endregion

    #region GettersAndSetter
    public int TrapPrice {
        get { return trapPrice; }
    }

    public GameManager GameManager {
        get { return gameManager; } 
        set { gameManager = value; }
    }

    public GameObject[] Objects
    {
        get { return objects; }
    }

    public TrapSetter[] TrapSetters
    {
        get { return trapSetters; }
    }
    #endregion

    // Use this for initialization
    void Start() {
        objects = GameObject.FindGameObjectsWithTag("Path");
        trapSetters = FindObjectsOfType<TrapSetter>();
    }

    public void PayForTrap(bool isPlayerOne) {
        gameManager.substractMonet(trapPrice, isPlayerOne);
    }

    public void DestroyAllTraps() {
        foreach (Trap trap in trapList) {
            Destroy(trap.gameObject);
        }

        trapList.Clear();
    }

    public void AddTrap(Trap trap)
    {
        trapList.Add(trap);
    }
}