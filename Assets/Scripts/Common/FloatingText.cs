﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour {

    private float destroyTime = 1.5f;

    void Start() {
        GetComponent<MeshRenderer>().sortingLayerName = "ObjectsOnMap";
        GetComponent<MeshRenderer>().sortingOrder = 6;
        Destroy(gameObject, destroyTime);
    }
}