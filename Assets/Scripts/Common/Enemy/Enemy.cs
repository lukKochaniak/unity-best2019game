﻿using Assets.Scripts.Common;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
    #region SerializeFields
    [SerializeField]
    private Transform[] curvesStart;
    [SerializeField]
    private float navigationUpdate;
    [SerializeField]
    private float healthPoints;
    [SerializeField]
    private int rewardAmonut;
    [SerializeField]
    private Transform healthBar;
    [SerializeField]
    private Slider healthBarFill;
    [SerializeField]
    private float healthBarFillOffset = -0.8f;
    [SerializeField]
    private bool isTargettingPlayerOne;
    [SerializeField]
    private List<Checkpoint> splits;
    [SerializeField]
    private int enemyType;
    #endregion

    #region Fields
    private GameManager gameManager;
    private bool facingRight = true;
    private bool movingForward = true;
    private int startIndex;
    private Transform[] currentWaypoints;
    [SerializeField]
    private Checkpoint targetCheckpoint;
    [SerializeField]
    private Checkpoint comingFromCheckpoint;
    private Transform enemy;
    private Collider2D enemyCollider;
    private Animator animator;
    private float navigationTime = 0;
    private bool isDead = false;
    private bool enabledWaypoints = false;
    private float currentHealth;
    private float speed = 0.8f;
    private bool isTargettingTower = false;
    private Vector3 floatingTextOffset = new Vector3(0.3f, 0.3f, 0);
    float floatingTextTime = 1.5f;
    private bool inCombat = false;
    private float attackTimer = 2f;
    private EnemyRange range;
    #endregion

    #region GettersAndSetter
    public bool IsDead {
        get { return isDead; }
    }

    public int StartIndex {
        get { return startIndex; }
        set { startIndex = value; }
    }

    public GameManager GameManager {
        get { return gameManager; }
        set { gameManager = value; }
    }

    public bool InCombat {
        get { return inCombat; }
        set { inCombat = value; }
    }

    #endregion


    // Use this for initialization
    void Start() {
        enemy = GetComponent<Transform>();
        enemyCollider = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        registerEnemyDependingOnGameMode();
        currentHealth = healthPoints;
        //possibleTowerTargets = new List<Tower>();
        if (SceneManager.GetActiveScene().name.Equals("Rush"))
            range = transform.GetChild(1).GetComponent<EnemyRange>();
    }

    // Update is called once per frame
    void Update() {
        if (!isDead) {
            navigationTime += Time.deltaTime;
            if (enemyType == 3) {
                navigationTime = navigationTime * 1.3f;
            }
            Vector3 oldPosition = transform.position;
            if (!inCombat) {
                if (navigationTime > navigationUpdate) {
                    if (enabledWaypoints) {
                        try {
                            enemy.position = Vector2.MoveTowards(enemy.position, targetCheckpoint.transform.position, speed * navigationTime);
                        } catch { }
                        if (SceneManager.GetActiveScene().name.Equals("Rush") && oldPosition == transform.position) {
                            findTarget(); //getting enemies unstuck in Rush mode
                        }
                    } else {
                        enemy.position = Vector2.MoveTowards(enemy.position, curvesStart[startIndex].position, speed * navigationTime);
                    }
                    navigationTime = 0;
                    bool movingRight;
                    if (transform.position.x >= oldPosition.x)
                        movingRight = true;
                    else
                        movingRight = false;
                    faceMovementDirection(movingRight);
                }
            } else {
                attack();
            }
        }
        PostionHealthBar();
    }

    public void attack() {
        if (attackTimer == 2) {
            animator.Play("OneAttacking");
            attackTimer -= Time.deltaTime;
        } else if (attackTimer <= 0)
            attackTimer = 2f;
        else
            attackTimer -= Time.deltaTime;
    }

    public void damageTarget() {
        range.Target.CurrentHealth--;
        FindObjectOfType<AudioManager>().PlaySound("SwordHit");
    }


    private void registerEnemyDependingOnGameMode() {
        gameManager.RegisterEnemy(this);
    }

    private void faceMovementDirection(bool movingRight) {
        if (movingRight && !facingRight) {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            facingRight = true;
        } else if (!movingRight && facingRight) {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            facingRight = false;
        }
    }

    private void findTarget() {
        if (movingForward) {
            comingFromCheckpoint = targetCheckpoint;
            targetCheckpoint = targetCheckpoint.Next[Random.Range(0, targetCheckpoint.Next.Count)];
        } else {
            comingFromCheckpoint = targetCheckpoint;
            if (targetCheckpoint.Next.Count == 1) {
                targetCheckpoint = targetCheckpoint.Previous;
            } else {
                if (isTargettingPlayerOne)
                    targetCheckpoint = targetCheckpoint.Next[0];
                else
                    targetCheckpoint = targetCheckpoint.Next[1];
                movingForward = true;
            }
        }
    }

    private void PostionHealthBar() {
        healthBarFill.value = currentHealth / healthPoints;
        Vector3 currentPos = transform.position;

        healthBar.position = new Vector3(currentPos.x, currentPos.y + healthBarFillOffset, currentPos.z);
        //healthBar.LookAt(Camera.main.transform);
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Checkpoint") {
            enabledWaypoints = true;
            if (targetCheckpoint == null)
                targetCheckpoint = other.GetComponent<Checkpoint>();
            if (other.GetComponent<Checkpoint>() == targetCheckpoint) {
                if (movingForward) {
                    int i = Random.Range(0, targetCheckpoint.Next.Count);
                    if (targetCheckpoint.Next.Count > 1 && !isTargettingTower && splits.Exists(x => x.gameObject.name == targetCheckpoint.gameObject.name)) {
                        if (i == 0)
                            isTargettingPlayerOne = true;
                        else
                            isTargettingPlayerOne = false;
                        isTargettingTower = true;
                    }
                    comingFromCheckpoint = targetCheckpoint;
                    targetCheckpoint = targetCheckpoint.Next[i];
                } else {
                    if (targetCheckpoint.Next.Count == 1) {
                        comingFromCheckpoint = targetCheckpoint;
                        targetCheckpoint = targetCheckpoint.Previous;
                    } else {
                        int i = targetCheckpoint.Next.FindIndex(x => comingFromCheckpoint == x);
                        comingFromCheckpoint = targetCheckpoint;
                        targetCheckpoint = targetCheckpoint.Next[Mathf.Abs(i - 1)];
                        movingForward = true;
                    }
                }
            }
        } else if (other.tag == "Finish") {
            determineFinishResultDependingOnGameMode();
        }

        if (other.tag == "PlayerOneFinish") {
            gameManager.substractMonet(rewardAmonut * 2, true);
            enemyEscapedRoutine();
            gameManager.showFloatingText(TowerManager.Instance.PLAYER_ONE_COLOR, "-" + rewardAmonut + "$", 36, transform.position, floatingTextTime);
        } else if (other.tag == "PlayerTwoFinish") {
            gameManager.substractMonet(rewardAmonut * 2, false);
            enemyEscapedRoutine();
            gameManager.showFloatingText(TowerManager.Instance.PLAYER_TWO_COLOR, "-" + rewardAmonut + "$", 36, transform.position, floatingTextTime);
        }

        if (other.tag == "Projectile") {
            Projectile newP = other.gameObject.GetComponent<Projectile>();
            enemyHit(newP.AttackStrength, newP.IsFromPlayerOne);
            Destroy(other.gameObject);
        } else if (other.tag == "MudTrap") {
            slowFromTrap();
        } else if (other.tag == "LavaTrap") {
            //enemyHit(100, true);
            getDamageFromTrap();
        }
    }

    private void enemyEscapedRoutine() {
        gameManager.RoundEscaped++;
        gameManager.TotalEscaped++;
        gameManager.UnregisterEnemy(this);
        gameManager.isWaveOver();
    }

    private void getDamageFromTrap() {
        float damage = 0.2f * currentHealth;
        currentHealth -= damage;
    }

    private void slowFromTrap() {
        speed /= 2;
    }

    private void determineFinishResultDependingOnGameMode() {
        if (SceneManager.GetActiveScene().name.Equals("Co-op")) {
            GameManagerCoop.Instance.Health--;
        }
        enemyEscapedRoutine();
    }

    public void enemyHit(int hitpoints, bool isFromPlayerOne) {
        gameManager.DamageHandler(hitpoints, isFromPlayerOne);

        if (SceneManager.GetActiveScene().name.Equals("Rush") && isTargettingTower) {
            if (inCombat) {
                inCombat = false;
                attackTimer = 2f;
                targetCheckpoint = comingFromCheckpoint;
                isTargettingPlayerOne = !isTargettingPlayerOne;
                movingForward = false;
                comingFromCheckpoint = targetCheckpoint.Next[0];
            } else {
                if (isFromPlayerOne && isTargettingPlayerOne) {
                    isTargettingPlayerOne = false;
                    if (movingForward) {
                        movingForward = false;
                        targetCheckpoint = comingFromCheckpoint;
                        comingFromCheckpoint = targetCheckpoint.Next[0];
                    } else {
                        movingForward = true;
                        comingFromCheckpoint = targetCheckpoint;
                        targetCheckpoint = targetCheckpoint.Next[1];
                    }
                } else if (!isFromPlayerOne && !isTargettingPlayerOne) {
                    isTargettingPlayerOne = true;
                    //comingFromCheckpoint = targetCheckpoint;
                    if (movingForward) {
                        movingForward = false;
                        Checkpoint temp = targetCheckpoint;
                        targetCheckpoint = comingFromCheckpoint;
                        comingFromCheckpoint = temp;
                    } else {
                        movingForward = true;
                        comingFromCheckpoint = targetCheckpoint;
                        targetCheckpoint = targetCheckpoint.Next[0];
                    }
                }
            }
        }
        if (currentHealth - hitpoints > 0) {
            currentHealth -= hitpoints;
            animator.Play("OneHurt");
        } else {
            currentHealth = 0;
            die(isFromPlayerOne);
        }
        determineAccuracy(hitpoints, isFromPlayerOne);

    }

    private void determineAccuracy(int strength, bool isPlayerOne) {
        if (isPlayerOne) {
            gameManager.TotalPlayerOneHits++;
            gameManager.TotalPlayerOneDamage += strength;
        } else {
            gameManager.TotalPlayerTwoHits++;
            gameManager.TotalPlayerTwoDamage += strength;
        }
    }

    public void die(bool isFromPlayerOne) {
        isDead = true;
        FindObjectOfType<AudioManager>().PlaySound("EnemyDieSong");
        animator.SetTrigger("didDie");
        enemyCollider.enabled = false;
        determineDieResultDependingOnGameMode(isFromPlayerOne);
    }

    private void determineDieResultDependingOnGameMode(bool isFromPlayerOne) {
        gameManager.TotalKilled++;
        gameManager.addMoney(rewardAmonut, isFromPlayerOne);
        gameManager.isWaveOver();
        Color color;
        if (isFromPlayerOne) {
            gameManager.PlayerOneTotalKilled++;
            color = TowerManager.Instance.PLAYER_ONE_COLOR;
        } else {
            gameManager.PlayerTwoTotalKilled++;
            color = TowerManager.Instance.PLAYER_TWO_COLOR;
        }
        gameManager.showFloatingText(color, "+" + rewardAmonut + "$", 36, transform.position + floatingTextOffset, floatingTextTime);
    }

}
