﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour {

    private Enemy parent;
    private Tower target;
    
    public Tower Target {
        get { return target; }
    }
    void Start () {
        parent = transform.parent.GetComponent<Enemy>();
	}
	

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "PlayerOneTower" || collision.tag == "PlayerTwoTower") {
            transform.parent.GetComponent<Enemy>().InCombat = true;
            target = collision.GetComponent<Tower>();
        }
    }
}
