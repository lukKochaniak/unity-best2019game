﻿using Assets.Scripts.Common;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class TowerManager : Singleton<TowerManager> {
    #region Constants
    private const int CASTLE_TOWER_PRICE = 10;
    private const int KNIGHT_TOWER_PRICE = 15;
    private const float TWO_DEGREES_TO_LEFT_ROTATION = -2f;
    private const float TWO_DEGREES_TO_RIGHT_ROTATION = 2f;
    private Color P_ONE_COLOR = Color.blue;
    private Color P_TWO_COLOR = Color.green;
    #endregion

    #region SerializeFields
    [SerializeField]
    private Tower rockTower;
    [SerializeField]
    private Tower castleTower;
    [SerializeField]
    private Tower knightTower;
    [SerializeField]
    private GameObject castleTowerShadow;
    [SerializeField]
    private GameObject knightTowerShadow;
    [SerializeField]
    private List<Tower> playerOneTowers;
    [SerializeField]
    private List<Tower> playerTwoTowers;
    [SerializeField]
    private GameManager gameManager;
    #endregion

    #region Fields
    public TowerBtn towerBtnPressed { get; set; }
    private List<Tower> TowerList = new List<Tower>();
    private int playerOneTowerIndex = 0;
    private int playerTwoTowerIndex = 0;
    private float changingCooldownPlayerOne = 0;
    private float changingCooldownPlayerTwo = 0;
    private float aimingCooldownPlayerOne = 0;
    private float aimingCooldownPlayerTwo = 0;
    private GameObject currentShadowPOne;
    private GameObject currentShadowPTwo;
    #endregion

    #region GettersAndSetters
    public List<Tower> PlayerOneTowers {
        get { return playerOneTowers; }
    }

    public Color PLAYER_ONE_COLOR {
        get { return P_ONE_COLOR; }
    }

    public Color PLAYER_TWO_COLOR {
        get { return P_TWO_COLOR; }
    }

    public List<Tower> PlayerTwoTowers {
        get { return playerTwoTowers; }
    }

    public GameManager GameManager {
        get { return gameManager; }
    }
    #endregion

    // Use thisClass1.cs for initialization
    void Start() {
        playerOneTowers[playerOneTowerIndex].SetActive();
        playerTwoTowers[playerTwoTowerIndex].SetActive();
        hideHealthBarsInCoopAndPvp();
        rotateAllPlayerTwoTowers();
    }

    // Update is called once per frame
    void Update() {
        controlTowers();
        analogCooldownControl();
        highlightTowerUpgrade();
    }

    private void highlightTowerUpgrade() {
        if(currentShadowPOne == null) {
            if (gameManager.PlayerOneMoney >= 10 && gameManager.PlayerOneMoney < 15) {
                Vector2 position = playerOneTowers[playerOneTowerIndex].transform.position;
                position.y += 0.8f;
                currentShadowPOne = Instantiate(castleTowerShadow, transform.position, transform.rotation);
                currentShadowPOne.transform.position = position;
            }
            if (gameManager.PlayerOneMoney >= 15) {
                Vector2 position = playerOneTowers[playerOneTowerIndex].transform.position;
                position.y += 0.8f;
                currentShadowPOne = Instantiate(knightTowerShadow, transform.position, transform.rotation);
                currentShadowPOne.transform.position = position;
            }
        }

        if(currentShadowPTwo == null) {
            if (gameManager.PlayerTwoMoney >= 10 && gameManager.PlayerTwoMoney < 15) {
                Vector2 position = playerTwoTowers[playerTwoTowerIndex].transform.position;
                position.y += 0.8f;
                currentShadowPTwo = Instantiate(castleTowerShadow, transform.position, transform.rotation);
                currentShadowPTwo.transform.position = position;
            }
            if (gameManager.PlayerTwoMoney >= 15) {
                Vector2 position = playerTwoTowers[playerTwoTowerIndex].transform.position;
                position.y += 0.8f;
                currentShadowPTwo = Instantiate(knightTowerShadow, transform.position, transform.rotation);
                currentShadowPTwo.transform.position = position;
            }
        }
    }

    private void analogCooldownControl() {
        changingCooldownPlayerOne -= Time.deltaTime;
        changingCooldownPlayerTwo -= Time.deltaTime;
        aimingCooldownPlayerOne -= Time.deltaTime;
        aimingCooldownPlayerTwo -= Time.deltaTime;
        if (changingCooldownPlayerOne < 0)
            changingCooldownPlayerOne = 0;
        if (changingCooldownPlayerTwo < 0)
            changingCooldownPlayerTwo = 0;
        if (aimingCooldownPlayerOne < 0)
            aimingCooldownPlayerOne = 0;
        if (aimingCooldownPlayerTwo < 0)
            aimingCooldownPlayerTwo = 0;
    }

    private void hideHealthBarsInCoopAndPvp() {
        string gameMode = determineGameModeOnSceneName();
        if (!gameMode.Equals("Rush")) {
            foreach (Tower tower in PlayerOneTowers)
                tower.HealthBarOffset = 100f;
            foreach (Tower tower in PlayerTwoTowers)
                tower.HealthBarOffset = -100f;
        }
    }

    private string determineGameModeOnSceneName() {
        if (SceneManager.GetActiveScene().name.Equals("Co-op")) {
            return "Co-op";
        } else if (SceneManager.GetActiveScene().name.Equals("PvpLevel2")) {
            return "PvpLevel2";
        } else if (SceneManager.GetActiveScene().name.Equals("PvpLevel1")) {
            return "Pvp";
        } else if (SceneManager.GetActiveScene().name.Equals("Rush")) {
            return "Rush";
        }
        return null;
    }


    public void DestroyAllTowers() {
        foreach (Tower tower in TowerList) {
            Destroy(tower.gameObject);
        }
        TowerList.Clear();
    }

    public void buyTower(int price, bool isPlayerOne) {
        gameManager.substractMonet(price, isPlayerOne);
    }

    private void controlTowers() {
        joystickPlayerOneMovements();
        playerOneKeyboardMovements();

        joystickPlayerTwoMovements();
        playerTwoKeyboardMovements();
    }

    private void determineAccuracy(bool isPlayerOne) {
        gameManager.addPlayerTotalShots(isPlayerOne);
    }

    private void destroyShadowPOne() {
        if(currentShadowPOne!=null)
            Destroy(currentShadowPOne.gameObject);
    }

    private void destroyShadowPTwo() {
        if (currentShadowPTwo != null)
            Destroy(currentShadowPTwo.gameObject);
    }

    #region JoystickAndKeyboard
    private void joystickPlayerOneMovements() {
        if (Input.GetAxis("1PS4_L_Horizontal") > 0.5 && changingCooldownPlayerOne == 0) {
            destroyShadowPOne();
            playerOneTowerIndex = highlightOfTowerRightMoveAndReturnIndex(playerOneTowers, playerOneTowerIndex);
            changingCooldownPlayerOne = 0.2f;
        }
        if (Input.GetAxis("1PS4_L_Horizontal") < -0.5 && changingCooldownPlayerOne == 0) {
            destroyShadowPOne();
            playerOneTowerIndex = highlightOfTowerLeftMoveAndReturnIndex(playerOneTowers, playerOneTowerIndex);
            changingCooldownPlayerOne = 0.2f;
        }
        if (Input.GetButtonDown("1PS4_R1")) {
            updateToKnightTower(true);
        }
        if (Input.GetButtonDown("1PS4_L1")) {
            updateToCastleTower(true);
        }
        if (Input.GetAxis("1PS4_R2") > 0.1) {
            bool wasFired = playerOneTowers[playerOneTowerIndex].Attack();
            if (wasFired)
                determineAccuracy(true);
        }
        if (Input.GetAxis("1PS4_L2") > 0.1 && aimingCooldownPlayerOne == 0) {
            playerOneTowers[playerOneTowerIndex].Aim.showCrosshair();
            aimingCooldownPlayerOne = 1.1f;
        }
        if (Input.GetAxis("1PS4_R_Horizontal") < -0.1 || Input.GetAxis("1PS4_R_Horizontal") > 0.1 || 
            Input.GetAxis("1PS4_R_Vertical") < -0.1 || Input.GetAxis("1PS4_R_Vertical") > 0.1) {
            float h1 = Input.GetAxis("1PS4_R_Horizontal");
            float v1 = Input.GetAxis("1PS4_R_Vertical");
            Vector3 angle = new Vector3(0f, 0f, 180 + Mathf.Atan2(h1, v1) * 180 / Mathf.PI);
            playerOneTowers[playerOneTowerIndex].Aim.Rotate(angle);
        }
    }

    private void joystickPlayerTwoMovements() {
        if (Input.GetAxis("2PS4_L_Horizontal") > 0.5 && changingCooldownPlayerTwo == 0) {
            destroyShadowPTwo();
            playerTwoTowerIndex = highlightOfTowerRightMoveAndReturnIndex(playerTwoTowers, playerTwoTowerIndex);
            changingCooldownPlayerTwo = 0.2f;
        }
        if (Input.GetAxis("2PS4_L_Horizontal") < -0.5 && changingCooldownPlayerTwo == 0) {
            destroyShadowPTwo();
            playerTwoTowerIndex = highlightOfTowerLeftMoveAndReturnIndex(playerTwoTowers, playerTwoTowerIndex);
            changingCooldownPlayerTwo = 0.2f;
        }
        if (Input.GetButtonDown("2PS4_R1")) {
            updateToKnightTower(false);
        }
        if (Input.GetButtonDown("2PS4_L1")) {
            updateToCastleTower(false);
        }
        if (Input.GetAxis("2PS4_R2") > 0.1) {
            bool wasFired = playerTwoTowers[playerTwoTowerIndex].Attack();
            if (wasFired)
                determineAccuracy(false);
        }
        if (Input.GetAxis("2PS4_L2") > 0.1 && aimingCooldownPlayerTwo == 0) {
            playerTwoTowers[playerTwoTowerIndex].Aim.showCrosshair();
            aimingCooldownPlayerTwo = 1.1f;
        }
        if (Input.GetAxis("2PS4_R_Horizontal") < -0.1 || Input.GetAxis("2PS4_R_Horizontal") > 0.1 ||
            Input.GetAxis("2PS4_R_Vertical") < -0.1 || Input.GetAxis("2PS4_R_Vertical") > 0.1) {
            float h1 = Input.GetAxis("2PS4_R_Horizontal");
            float v1 = Input.GetAxis("2PS4_R_Vertical");
            Vector3 angle = new Vector3(0f, 0f, 180 + Mathf.Atan2(h1, v1) * 180 / Mathf.PI);
            playerTwoTowers[playerTwoTowerIndex].Aim.Rotate(angle);
        }
    }

    private void playerOneKeyboardMovements() {
        if (Input.GetKeyDown(KeyCode.S)) {
            playerOneTowerIndex = highlightOfTowerLeftMoveAndReturnIndex(playerOneTowers, playerOneTowerIndex);
        } else if (Input.GetKeyDown(KeyCode.W)) {
            playerOneTowerIndex = highlightOfTowerRightMoveAndReturnIndex(playerOneTowers, playerOneTowerIndex);
        } else if (Input.GetKeyDown(KeyCode.Space)) {
            bool wasFired = playerOneTowers[playerOneTowerIndex].Attack();
            if (wasFired)
                determineAccuracy(true);
        } else if (Input.GetKeyDown(KeyCode.Q)) {
            updateToCastleTower(true);
        } else if (Input.GetKeyDown(KeyCode.E)) {
            updateToKnightTower(true);
        } else if (Input.GetKey(KeyCode.A)) {
            playerOneTowers[playerOneTowerIndex].Aim.Rotate(TWO_DEGREES_TO_LEFT_ROTATION);
        } else if (Input.GetKey(KeyCode.D)) {
            playerOneTowers[playerOneTowerIndex].Aim.Rotate(TWO_DEGREES_TO_RIGHT_ROTATION);
        }
    }

    private void playerTwoKeyboardMovements() {
        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            playerTwoTowerIndex = highlightOfTowerLeftMoveAndReturnIndex(playerTwoTowers, playerTwoTowerIndex);
        } else if (Input.GetKeyDown(KeyCode.UpArrow)) {
            playerTwoTowerIndex = highlightOfTowerRightMoveAndReturnIndex(playerTwoTowers, playerTwoTowerIndex);
        } else if (Input.GetKeyDown(KeyCode.RightControl)) {
            bool wasFired = playerTwoTowers[playerTwoTowerIndex].Attack();
            if (wasFired)
                determineAccuracy(false);
        } else if (Input.GetKeyDown(KeyCode.Period)) {
            updateToCastleTower(false);
        } else if (Input.GetKeyDown(KeyCode.Slash)) {
            updateToKnightTower(false);
        } else if (Input.GetKey(KeyCode.RightArrow)) {
            playerTwoTowers[playerTwoTowerIndex].Aim.Rotate(TWO_DEGREES_TO_LEFT_ROTATION);
        } else if (Input.GetKey(KeyCode.LeftArrow)) {
            playerTwoTowers[playerTwoTowerIndex].Aim.Rotate(TWO_DEGREES_TO_RIGHT_ROTATION);
        }
    }
    #endregion

    private void rotateAllPlayerTwoTowers() {
        foreach (Tower tower in playerTwoTowers) {
            tower.Aim.Rotate(180f);
        }
    }

    private int highlightOfTowerLeftMoveAndReturnIndex(List<Tower> playerTowers, int playerIndex) {
        playerTowers[playerIndex].SetInactive();
        playerIndex--;
        if (playerIndex < 0)
            playerIndex = playerTowers.Count - 1;
        playerTowers[playerIndex].SetActive();
        return playerIndex;
    }

    private int highlightOfTowerRightMoveAndReturnIndex(List<Tower> playerTowers, int playerIndex) {
        playerTowers[playerIndex].SetInactive();
        playerIndex++;
        if (playerIndex == playerTowers.Count)
            playerIndex = 0;
        playerTowers[playerIndex].SetActive();
        return playerIndex;
    }

    #region UpgradeTower
    private void updateToCastleTower(bool isPlayerOne) {
        if (CASTLE_TOWER_PRICE <= gameManager.PlayerOneMoney && isPlayerOne) {
            upgradeToTower(CASTLE_TOWER_PRICE, castleTower, playerOneTowers, playerOneTowerIndex, true);
        } else if (CASTLE_TOWER_PRICE <= gameManager.PlayerTwoMoney && !isPlayerOne) {
            upgradeToTower(CASTLE_TOWER_PRICE, castleTower, playerTwoTowers, playerTwoTowerIndex, false);
        }
    }

    private void updateToKnightTower(bool isPlayerOne) {
        if (KNIGHT_TOWER_PRICE <= gameManager.PlayerOneMoney && isPlayerOne) {
            upgradeToTower(KNIGHT_TOWER_PRICE, knightTower, playerOneTowers, playerOneTowerIndex, true);
        } else if (KNIGHT_TOWER_PRICE <= gameManager.PlayerTwoMoney && !isPlayerOne) {
            upgradeToTower(KNIGHT_TOWER_PRICE, knightTower, playerTwoTowers, playerTwoTowerIndex, false);
        }

    }

    private void upgradeToTower(int towerPrice, Tower typeOfTowerToUpgrade, List<Tower> playerTowers, int playerTowerIndex, bool isPlayerOne) {
        string gameMode = determineGameModeOnSceneName();
        if (isPlayerOne) {
            destroyShadowPOne();
        } else {
            destroyShadowPTwo();
        }
        buyTower(towerPrice, isPlayerOne);
        Transform transform = playerTowers[playerTowerIndex].transform;
        Vector3 aimPosition = playerTowers[playerTowerIndex].Aim.transform.position;
        Quaternion aimRotation = playerTowers[playerTowerIndex].Aim.transform.rotation;
        Destroy(playerTowers[playerTowerIndex].gameObject);
        playerTowers[playerTowerIndex] = Instantiate(typeOfTowerToUpgrade, transform.position, transform.rotation);
        playerTowers[playerTowerIndex].Aim.transform.position = aimPosition;
        playerTowers[playerTowerIndex].Aim.transform.rotation = aimRotation;
        if (!isPlayerOne) {
            playerTowers[playerTowerIndex].tag = "PlayerTwoTower";
            playerTowers[playerTowerIndex].IsPlayerOneTower = false;
            if (!gameMode.Equals("Rush"))
                playerTowers[playerTwoTowerIndex].HealthBarOffset = -100f;
            else
                playerTowers[playerTwoTowerIndex].HealthBarOffset = -0.8f;
            playerTowers[playerTwoTowerIndex].CooldownBarOffset = -0.9f;
        } else {
            playerTowers[playerTowerIndex].tag = "PlayerOneTower";
            playerTowers[playerTowerIndex].IsPlayerOneTower = true;
            if (!gameMode.Equals("Rush"))
                playerTowers[playerOneTowerIndex].HealthBarOffset = 100f;
        }
        playerTowers[playerTowerIndex].SetActive();
    }
    #endregion

}
