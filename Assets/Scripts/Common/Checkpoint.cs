﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour{
    [SerializeField]
    List<Checkpoint> next;
    Checkpoint previous { get; set; }

    public List<Checkpoint> Next {
        get { return next;  }
        set { Next = value; }
    }


    public Checkpoint Previous {
        get { return previous; }
    }

    private void Start() {
        foreach(Checkpoint checkpoint in next) {
            checkpoint.previous = this;
        }
    }

}
