﻿using UnityEngine;

public class TowerBtn : MonoBehaviour {

    #region SerializeFields
    [SerializeField]
    private Tower towerObject;
    [SerializeField]
    private Sprite dragSprite;
    [SerializeField]
    private int towerPrice;
    #endregion

    #region GettersAndSetters
    public int TowerPrice {
        get { return towerPrice; }
        set { towerPrice = value; }
    }

    public Tower TowerObject {
        get { return towerObject; }
    }

    public Sprite DragSprite {
        get { return dragSprite; }
    }
    #endregion
}
