﻿using System.Collections.Generic;
using UnityEngine;

public class Range : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private List<Enemy> enemiesInRange;

    public List<Enemy> EnemiesInRange {
        get { return enemiesInRange; }
    }


    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        enemiesInRange = new List<Enemy>();
    }

    public void Select() {
        spriteRenderer.enabled = !spriteRenderer.enabled;
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Enemy") {
            enemiesInRange.Remove(other.GetComponent<Enemy>());
            //Debug.Log("Enemies in Range: " + enemiesInRange.Count);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Enemy") {
            enemiesInRange.Add(other.GetComponent<Enemy>());
            //Debug.Log("Enemies in Range: " + enemiesInRange.Count);
        }
    }
}
