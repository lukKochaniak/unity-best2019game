﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Common;

public class Tower : MonoBehaviour {

    #region SerializeFields
    [SerializeField]
    private float timeBetweenAttacks;
    [SerializeField]
    private Projectile projectile;
    [SerializeField]
    private Transform cooldownBar;
    [SerializeField]
    private Slider cooldownFill;
    [SerializeField]
    private float cooldownBarOffset = -0.8f;
    [SerializeField]
    private Transform healthBar;
    [SerializeField]
    private Slider healthFill;
    [SerializeField]
    private float healthBarOffset = -0.9f;
    [SerializeField]
    private bool isPlayerOneTower;
    [SerializeField]
    private float maxHealth;
    #endregion

    #region Fields
    private Enemy targetEnemy = null;
    private float attackCounter;
    private bool isAttacking = false;
    private bool isActive = false;
    private float warningCounter = 2f;

    private SpriteRenderer spriteRenderer;
    private Material material;
    private Color normalColor;
    private Color selectedColor;
    private Range range;
    private Aim aim;
    #endregion

    #region GettersAndSetters
    private float currentHealth { get; set; }

    public Aim Aim {
        get { return aim; }
    }

    public float CurrentHealth {
        get { return currentHealth; }
        set { currentHealth = value; }
    }

    public bool IsPlayerOneTower {
        get { return isPlayerOneTower; }
        set { isPlayerOneTower = value; }
    }

    public float CooldownBarOffset {
        get { return cooldownBarOffset; }
        set { cooldownBarOffset = value; }
    }

    public float HealthBarOffset {
        get { return healthBarOffset; }
        set { healthBarOffset = value; }
    }
    #endregion

    void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        range = transform.GetChild(0).GetComponent<Range>();
        aim = transform.GetChild(1).GetComponent<Aim>();
        currentHealth = maxHealth;
        spriteRenderer.material.SetFloat("_Outline", 0);

        normalColor = spriteRenderer.color;
    }

    void Update() {
        attackCounter -= Time.deltaTime;
        PositionCooldownBar();
        PositionHealthBar();
        if (currentHealth / maxHealth <= 0.25)
            showWarning();
    }

    private void PositionCooldownBar() {
        cooldownFill.value = attackCounter / timeBetweenAttacks;
        Vector3 currentPos = transform.position;

        cooldownBar.position = new Vector3(currentPos.x, currentPos.y + cooldownBarOffset, currentPos.z);
        //cooldownBar.LookAt(Camera.main.transform);
    }
    
    public void showWarning() {
        if (TowerManager.Instance.GameManager.CurrentState != gameStatus.gameover && TowerManager.Instance.GameManager.CurrentState != gameStatus.win) {
            bool displayedWarning = true;
            if (warningCounter >= 2)
                displayedWarning = false;
            if (!displayedWarning) {
                TowerManager.Instance.GameManager.showFloatingText(Color.red, "!", 128, transform.position + new Vector3(1f, 0f), 1f);
                FindObjectOfType<AudioManager>().PlaySound("Warning");
                displayedWarning = false;
                warningCounter -= Time.deltaTime;
            } else if (warningCounter <= 0)
                warningCounter = 2f;
            else
                warningCounter -= Time.deltaTime;
        }


    }

    private void PositionHealthBar() {
        healthFill.value = currentHealth / maxHealth;
        Vector3 currentPos = transform.position;

        healthBar.position = new Vector3(currentPos.x, currentPos.y + healthBarOffset, currentPos.z);
        //cooldownBar.LookAt(Camera.main.transform);
    }


    public bool Attack() {
        if (isActive && attackCounter <= 0f) {
            FindObjectOfType<AudioManager>().PlaySound("TowerShootingSound");

            attackCounter = timeBetweenAttacks;
            Projectile newProjectile = Instantiate(projectile) as Projectile;
            newProjectile.IsFromPlayerOne = isPlayerOneTower;
            newProjectile.transform.parent = this.transform;
            newProjectile.transform.position = transform.position;
            StartCoroutine(MoveProjectile(newProjectile));
            return true;
        }
        return false;
    }

    IEnumerator MoveProjectile(Projectile projectile) {
        var direction = aim.transform.position - transform.position;
        var angleDirection = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        while (projectile != null) {
            projectile.transform.rotation = Quaternion.AngleAxis(angleDirection, Vector3.forward);
            Vector2 position = projectile.transform.position;
            position.x += direction.x / 5;
            position.y += direction.y / 5;
            projectile.transform.position = position;
            yield return null;
        }
    }

    public void SetActive() {
        Color color;
        if (isPlayerOneTower)
            color = TowerManager.Instance.PLAYER_ONE_COLOR;
        else
            color = TowerManager.Instance.PLAYER_TWO_COLOR;
        isActive = true;
        range.Select();
        spriteRenderer.material.SetFloat("_Outline", 5);
        spriteRenderer.material.SetColor("_Color", color);
    }

    public void SetInactive() {
        isActive = false;
        range.Select();
        spriteRenderer.material.SetFloat("_Outline", 0);
        spriteRenderer.material.SetColor("_Color", Color.clear);
    }
}
