﻿using System.Collections;
using UnityEngine;

public class Aim : MonoBehaviour {

    //private float speed;
    private Vector3 zAxis = new Vector3(0, 0, 1);
    private Transform target;
    [SerializeField]
    private SpriteRenderer crosshair;
    private Color startingColor;
    private float defaultSpeed = 2f;

    void Start() {
        target = transform.parent;
        startingColor = crosshair.material.color;
    }

    private IEnumerator FadeOut() {
        for(float f = 1f; f>= -0.05f; f -= 0.05f) {
            Color c = crosshair.material.color;
            c.a = f;
            crosshair.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void fadeOut() {
        if(startingColor == null) {
            startingColor = crosshair.material.color;
        }
        StartCoroutine(FadeOut());
    }

    public void showCrosshair() {
        crosshair.material.color = startingColor;
        crosshair.gameObject.SetActive(true);
        fadeOut();
    }

    public void Rotate(float speed) {
        if (target == null) {
            target = transform.parent;
        }
        transform.RotateAround(target.position, zAxis, speed);
    }

    public void Rotate(Vector3 angle) {
        if (target == null) {
            target = transform.parent;
        }
        Vector3 diff = angle - transform.localEulerAngles;
        if ((diff.z > 1 && diff.z < 180) || diff.z <= -180)
            transform.RotateAround(target.position, zAxis, defaultSpeed);
        if ((diff.z < -1 && diff.z > -180) || diff.z >= 180)
            transform.RotateAround(target.position, zAxis, -defaultSpeed);
    }

}
