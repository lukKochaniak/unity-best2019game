﻿using UnityEngine;

public enum proType {
    rock, arrow, fireball
};

public class Projectile : MonoBehaviour {

    [SerializeField]
    private int attackStrength;
    [SerializeField]
    private proType projectileType;
    private bool isFromPlayerOne;

    #region GettersAndSetters
    public int AttackStrength {
        get { return attackStrength; }
    }

    public proType ProjectileType {
        get { return projectileType; }
    }

    public bool IsFromPlayerOne {
        get { return isFromPlayerOne; }
        set { isFromPlayerOne = value; }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Enemy")
            Destroy(this.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Range" && other.transform.parent == transform.parent) {
            Destroy(this.gameObject);
        }
    }
}
