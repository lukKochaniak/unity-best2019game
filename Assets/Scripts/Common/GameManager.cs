﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Common {
    public abstract class GameManager : MonoBehaviour {
        #region SerializeFields
        [SerializeField]
        private TextMeshPro floatingText;
        [SerializeField]
        private int totalWaves = 10;
        [SerializeField]
        private Text playerOneMoneyLbl;
        [SerializeField]
        private Text playerTwoMoneyLbl;
        [SerializeField]
        private Text currentWaveLbl;
        [SerializeField]
        private Text playBtnLbl;
        [SerializeField]
        private Button playBtn;
        [SerializeField]
        private Image currentWaveLblObj;
        [SerializeField]
        private GameObject[] spawnPoints;
        [SerializeField]
        private Enemy[] minions;
        [SerializeField]
        private int totalEnemies = 2;
        [SerializeField]
        private int enemiesPerSpawn;
        [SerializeField]
        private GameObject firstPlayerReadyLabel;
        [SerializeField]
        private GameObject secondPlayerReadyLabel;
        [SerializeField]
        private TextMeshProUGUI countingLabel;
        [SerializeField]
        private TextMeshProUGUI playerOneMonstersKilled;
        [SerializeField]
        private TextMeshProUGUI playerTwoMonstersKilled;
        [SerializeField]
        private TextMeshProUGUI playerOneTotalDamage;
        [SerializeField]
        private TextMeshProUGUI playerTwoTotalDamage;
        [SerializeField]
        private TextMeshProUGUI playerOneMoneyEarned;
        [SerializeField]
        private TextMeshProUGUI playerTwoMoneyEarned;
        [SerializeField]
        private TextMeshProUGUI playerOneAccuracy;
        [SerializeField]
        private TextMeshProUGUI playerTwoAccuracy;
        [SerializeField]
        private Slider playerOneAccuracySlider;
        [SerializeField]
        private Slider playerTwoAccuracySlider;
        #endregion

        #region Fields
        private int enemiesSpawned = 0;
        private int waveNumber = 1;
        private int playerOneMoney = 10;
        private int playerTwoMoney = 10;
        private int totalPlayerOneDamage = 0;
        private int totalPlayerTwoDamage = 0;
        private int totalPlayerOneEarnedMoney = 0;
        private int totalPlayerTwoEarnedMoney = 0;
        private int totalPlayerOneShots = 0;
        private int totalPlayerTwoShots = 0;
        private int totalPlayerOneHits = 0;
        private int totalPlayerTwoHits = 0;
        private double totalPlayerOneAccuracy = 0;
        private double totalPlayerTwoAccuracy = 0;
        private int totalEscaped = 0;
        private int roundEscaped = 0;
        private int totalKilled = 0;
        private int whichEnemiesToSpawn = 0;
        private int enemiesToSpawn = 0;
        private gameStatus currentState = gameStatus.play;
        private bool firstTime = true;
        public volatile List<Enemy> EnemyList = new List<Enemy>();
        const float spawnDelay = 1f;
        public GameObject GameOverUI;
        private int playerOneTotalKilled = 0;
        private int playerTwoTotalKilled = 0;
        private bool firstPlayerIsReady = false;
        private bool secondPlayerIsReady = false;
        #endregion

        #region GettersAndSetters
        public gameStatus CurrentState {
            get { return currentState; }
            set { currentState = value; }
        }
        public int WaveNumber {
            get { return waveNumber; }
            set { waveNumber = value; }
        }
        public int TotalEscaped {
            get { return totalEscaped; }
            set { totalEscaped = value; }
        }

        public int RoundEscaped {
            get { return roundEscaped; }
            set { roundEscaped = value; }
        }

        public int TotalKilled {
            get { return totalKilled; }
            set { totalKilled = value; }
        }

        public int PlayerOneMoney {
            get { return playerOneMoney; }
            set { playerOneMoney = value; playerOneMoneyLbl.text = playerOneMoney.ToString(); }
        }

        public int PlayerTwoMoney {
            get { return playerTwoMoney; }
            set { playerTwoMoney = value; playerTwoMoneyLbl.text = playerTwoMoney.ToString(); }
        }

        public int PlayerOneTotalKilled {
            get { return playerOneTotalKilled; }
            set {
                playerOneTotalKilled = value; //playerOneTotalKilledLbl.text = playerOneTotalKilled.ToString(); 
            }
        }

        public int PlayerTwoTotalKilled {
            get { return playerTwoTotalKilled; }
            set {
                playerTwoTotalKilled = value;
                //playerTwoTotalKilledLbl.text = PlayerTwoTotalKilled.ToString();
            }
        }

        public int TotalPlayerOneShots {
            get { return totalPlayerOneShots; }
            set { totalPlayerOneShots = value; }
        }

        public int TotalPlayerTwoShots {
            get { return totalPlayerTwoShots; }
            set { totalPlayerTwoShots = value; }
        }

        public int TotalPlayerOneHits {
            get { return totalPlayerOneHits; }
            set { totalPlayerOneHits = value; }
        }

        public int TotalPlayerTwoHits {
            get { return totalPlayerTwoHits; }
            set { totalPlayerTwoHits = value; }
        }

        public int TotalPlayerOneDamage {
            get { return totalPlayerOneDamage; }
            set { totalPlayerOneDamage = value; }
        }

        public int TotalPlayerTwoDamage {
            get { return totalPlayerTwoDamage; }
            set { totalPlayerTwoDamage = value; }
        }

        public int TotalWaves {
            get { return totalWaves; }
            set { totalWaves = value; }
        }

        public Text PlayerOneMoneyLbl {
            get { return playerOneMoneyLbl; }
            set { playerOneMoneyLbl = value; }
        }

        public Text PlayerTwoMoneyLbl {
            get { return playerTwoMoneyLbl; }
            set { playerTwoMoneyLbl = value; }
        }

        public Text CurrentWaveLbl {
            get { return currentWaveLbl; }
            set { currentWaveLbl = value; }
        }

        public Text PlayBtnLbl {
            get { return playBtnLbl; }
            set { playBtnLbl = value; }
        }

        public Button PlayBtn {
            get { return playBtn; }
            set { playBtn = value; }
        }

        public Image CurrentWaveLblObj {
            get { return currentWaveLblObj; }
            set { currentWaveLblObj = value; }
        }

        public GameObject[] SpawnPoints {
            get { return spawnPoints; }
            set { spawnPoints = value; }
        }

        public Enemy[] Minions {
            get { return minions; }
            set { minions = value; }
        }

        public int TotalEnemies {
            get { return totalEnemies; }
            set { totalEnemies = value; }
        }

        public int EnemiesPerSpawn {
            get { return enemiesPerSpawn; }
            set { enemiesPerSpawn = value; }
        }

        public TextMeshProUGUI CountingLabel {
            get { return countingLabel; }
            set { countingLabel = value; }
        }


        public bool FirstTime {
            get { return firstTime; }
            set { firstTime = value; }
        }

        public TextMeshProUGUI PlayerOneMonstersKilled
        {
            get { return playerOneMonstersKilled; }
            set { playerOneMonstersKilled = value; }
        }

        public TextMeshProUGUI PlayerTwoMonstersKilled
        {
            get { return playerTwoMonstersKilled; }
            set { playerTwoMonstersKilled = value; }
        }

        public TextMeshProUGUI PlayerOneTotalDamage
        {
            get { return playerOneTotalDamage; }
            set { playerOneTotalDamage = value; }
        }

        public TextMeshProUGUI PlayerTwoTotalDamage
        {
            get { return playerTwoTotalDamage; }
            set { playerTwoTotalDamage = value; }
        }

        public TextMeshProUGUI PlayerOneMoneyEarned
        {
            get { return playerOneMoneyEarned; }
            set { playerOneMoneyEarned = value; }
        }

        public TextMeshProUGUI PlayerTwoMoneyEarned
        {
            get { return playerTwoMoneyEarned; }
            set { playerTwoMoneyEarned = value; }
        }

        public TextMeshProUGUI PlayerOneAccuracy
        {
            get { return playerOneAccuracy; }
            set { playerOneAccuracy = value; }
        }

        public TextMeshProUGUI PlayerTwoAccuracy
        {
            get { return playerTwoAccuracy; }
            set { playerTwoAccuracy = value; }
        }

        public Slider PlayerOneAccuracySlider
        {
            get { return playerOneAccuracySlider; }
            set { playerOneAccuracySlider = value; }
        }

        public Slider PlayerTwoAccuracySlider
        {
            get { return playerOneAccuracySlider; }
            set { playerOneAccuracySlider = value; }
        }
        #endregion

        public void joystickStartOrPSPressed() {

            if (Input.GetButtonDown("1PS4_PS") || Input.GetButtonDown("2PS4_PS")) {
                SceneManager.LoadScene("MainMenu");
                Time.timeScale = 1f;
            }
            if ((Input.GetButtonDown("1PS4_X") || Input.GetKeyDown(KeyCode.B)) && playBtn.gameObject.activeSelf) {
                setFirstPlayerReady();
            }
            if ((Input.GetButtonDown("2PS4_X") || Input.GetKeyDown(KeyCode.N)) && playBtn.gameObject.activeSelf) {
                setSecondPlayerReady();
            }
            if (playBtn.gameObject.activeSelf && firstPlayerIsReady && secondPlayerIsReady) {
                showCounting();
            }
        }

        IEnumerator Count() {
            countingLabel.gameObject.SetActive(true);
            for (int i = 3; i >= 0; i -= 1) {
                if (i == 0) {
                    FindObjectOfType<AudioManager>().PlaySound("CountGo");
                    countingLabel.text = "GO!";
                } else {
                    FindObjectOfType<AudioManager>().PlaySound("Count" + i.ToString());
                    countingLabel.text = i.ToString();
                }
                yield return new WaitForSeconds(1f);
            }
            countingLabel.gameObject.SetActive(false);
            playBtnPressed();
        }

        private void showCounting() {
            currentWaveLblObj.gameObject.SetActive(false);
            playBtn.gameObject.SetActive(false);
            firstPlayerReadyLabel.gameObject.SetActive(false);
            secondPlayerReadyLabel.gameObject.SetActive(false);
            
            StartCoroutine("Count");
        }

        private void setSecondPlayerReady() {
            secondPlayerIsReady = true;
            secondPlayerReadyLabel.gameObject.SetActive(true);
        }

        private void setFirstPlayerReady() {
            firstPlayerIsReady = true;
            firstPlayerReadyLabel.gameObject.SetActive(true);
        }

        public virtual IEnumerator spawn() {
            if (enemiesPerSpawn > 0 && EnemyList.Count < totalEnemies) {
                for (int i = 0; i < enemiesPerSpawn; i++) {
                    if (enemiesSpawned < totalEnemies) {
                        int index = UnityEngine.Random.Range(0, spawnPoints.Length);
                        Enemy newEnemy = Instantiate(minions[UnityEngine.Random.Range(0, enemiesToSpawn)]) as Enemy;
                        newEnemy.GameManager = this;
                        newEnemy.StartIndex = index;
                        newEnemy.transform.position = spawnPoints[index].transform.position;
                        enemiesSpawned++;
                        yield return new WaitForSeconds(0.5f);
                    }
                }
                yield return new WaitForSeconds(spawnDelay);
                StartCoroutine(spawn());
            }
        }

        public void RegisterEnemy(Enemy enemy) {
            EnemyList.Add(enemy);
        }

        public void UnregisterEnemy(Enemy enemy) {
            EnemyList.Remove(enemy);
            Destroy(enemy.gameObject);
            isWaveOver();
        }

        public void DestroyAllEnemies() {
            foreach (Enemy enemy in EnemyList) {
                Destroy(enemy.gameObject);
            }
            EnemyList.Clear();
        }

        public void addPlayerTotalShots(bool isPlayerOne) {
            if (isPlayerOne) {
                TotalPlayerOneShots++;
            } else {
                TotalPlayerTwoShots++;
            }
        }

        public void addMoney(int amount, bool isPlayerOne) {
            if (isPlayerOne) {
                PlayerOneMoney += amount;
                totalPlayerOneEarnedMoney += amount;
            } else {
                PlayerTwoMoney += amount;
                totalPlayerTwoEarnedMoney += amount;
            }
        }

        public void substractMonet(int amount, bool isPlayerOne) {
            if (isPlayerOne) {
                PlayerOneMoney -= amount;
                if (PlayerOneMoney < 0)
                    PlayerOneMoney = 0;
            } else {
                PlayerTwoMoney -= amount;
                if (PlayerTwoMoney < 0)
                    PlayerTwoMoney = 0;
            }
        }

        public void setAccuracy()
        {
            if (totalPlayerOneShots == 0)
            {
                totalPlayerOneAccuracy = 0;
            }
            else
            {
                totalPlayerOneAccuracy = System.Math.Round((float)totalPlayerOneHits / (float)totalPlayerOneShots * 100, 2);
            }

            if (totalPlayerTwoShots == 0)
            {
                totalPlayerTwoAccuracy = 0;
            }
            else
            {
                totalPlayerTwoAccuracy = System.Math.Round((float)totalPlayerTwoHits / (float)totalPlayerTwoShots * 100, 2);
            }
        }

        public void DamageHandler(int hitpoints, bool isFromPlayerOne) {
            if (isFromPlayerOne) {
                totalPlayerOneDamage += hitpoints;
            } else {
                totalPlayerTwoDamage += hitpoints;
            }
        }

        public void isWaveOver() {
            //totalEscaped
            if ((RoundEscaped + TotalKilled) == totalEnemies) {
                if (waveNumber <= minions.Length) {
                    enemiesToSpawn = waveNumber;
                }
                setCurrentGameState();
                showMenu();
            }
        }

        public virtual void setCurrentGameState() {
            if (waveNumber == 0 && (TotalKilled + RoundEscaped) == 0) {
                currentState = gameStatus.play;
            } else if (waveNumber >= totalWaves) {
                currentState = gameStatus.next;
            } else {
                currentState = gameStatus.next;
            }
        }

        public void setStats() {
            
            setAccuracy();

            playerOneAccuracySlider.value = (float)totalPlayerOneAccuracy / 100;
            playerTwoAccuracySlider.value = (float)totalPlayerTwoAccuracy / 100;
            playerOneMonstersKilled.text = "" + playerOneTotalKilled;
            playerTwoMonstersKilled.text = "" + playerTwoTotalKilled;
            playerOneTotalDamage.text = "" + totalPlayerOneDamage + "HP";
            playerTwoTotalDamage.text = "" + totalPlayerTwoDamage + "HP";
            playerOneMoneyEarned.text = "" + totalPlayerOneEarnedMoney + "$";
            playerTwoMoneyEarned.text = "" + totalPlayerTwoEarnedMoney + "$";
            playerOneAccuracy.text = "" + totalPlayerOneAccuracy + "%";
            playerTwoAccuracy.text = "" + totalPlayerTwoAccuracy + "%";

        }

        public virtual void showMenu() {
            switch (currentState) {
                case gameStatus.gameover:
                    FindObjectOfType<AudioManager>().StopPlayingSound("ThemeSong");
                    FindObjectOfType<AudioManager>().PlaySound("LosingGameSound");
                    if (firstTime) {
                        GameOverUI.SetActive(true);
                        firstTime = false;
                    }
                    setStats();
                    break;
                case gameStatus.next:
                    playBtnLbl.text = "Next Wave";
                    break;
                case gameStatus.play:
                    playBtnLbl.text = "Play";
                    break;
                case gameStatus.win:
                    playBtnLbl.text = "You've won, click this button to play again!";
                    FindObjectOfType<AudioManager>().PlaySound("WinningGameSound");
                    break;
            }
            playBtn.gameObject.SetActive(true);
            currentWaveLblObj.gameObject.SetActive(true);
        }

        public void playBtnPressed() {
            switch (currentState) {
                case gameStatus.next:
                    totalEnemies += waveNumber;
                    enemiesSpawned = 0;
                    break;
                default:
                    TotalEscaped = 0;
                    PlayerOneMoney = 10;
                    PlayerTwoMoney = 10;
                    PlayerOneTotalKilled = 0;
                    PlayerTwoTotalKilled = 0;
                    totalPlayerOneDamage = 0;
                    totalPlayerTwoDamage = 0;
                    totalPlayerOneEarnedMoney = 0;
                    totalPlayerTwoEarnedMoney = 0;
                    totalPlayerOneShots = 0;
                    totalPlayerTwoShots = 0;
                    totalPlayerOneHits = 0;
                    totalPlayerTwoHits = 0;
                    enemiesToSpawn = 0;
                    waveNumber = 0;
                    TowerManager.Instance.DestroyAllTowers();
                    playerOneMoneyLbl.text = PlayerOneMoney.ToString();
                    playerTwoMoneyLbl.text = PlayerTwoMoney.ToString();
                    firstTime = true;
                    break;
            }
            waveNumber++;
            DestroyAllEnemies();
            TrapManager.Instance.DestroyAllTraps();
            firstPlayerIsReady = false;
            secondPlayerIsReady = false;
            TotalKilled = 0;
            RoundEscaped = 0;
            currentWaveLbl.text = "Wave " + (waveNumber + 1) + " of " + totalWaves;
            StartCoroutine(spawn());
            currentWaveLblObj.gameObject.SetActive(false);
            playBtn.gameObject.SetActive(false);
            firstPlayerReadyLabel.gameObject.SetActive(false);
            secondPlayerReadyLabel.gameObject.SetActive(false);
            countingLabel.gameObject.SetActive(false);
        }

        public void showFloatingText(Color color, string msg, float size, Vector3 pos, float time) {
            TextMeshPro text = Instantiate(floatingText, pos, Quaternion.identity);
            text.fontSize = size;
            text.text = msg;
            text.faceColor = color;
            text.outlineWidth = 0.175f;
            Destroy(text.gameObject, time);
        }
    }
}
