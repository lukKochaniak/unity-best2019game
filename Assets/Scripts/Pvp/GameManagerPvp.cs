﻿using Assets.Scripts.Common;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerPvp : GameManager {
    #region SerializeFields
    [SerializeField]
    private Text playerOneTotalKilledLbl;
    [SerializeField]
    private Text playerTwoTotalKilledLbl;
    [SerializeField]
    private Text winnerText;
    [SerializeField]
    private GameObject WinnerUI;
    [SerializeField]
    private GameObject top5UI;
    [SerializeField]
    private Button top5Button;
    #endregion

    #region Singleton
    private static GameManagerPvp instance = null;

    public static GameManagerPvp Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<GameManagerPvp>();
            } else if (instance != FindObjectOfType<GameManagerPvp>()) {
                Destroy(FindObjectOfType<GameManagerPvp>());
            }
            return instance;
        }
    }
    #endregion


    // Use this for initialization
    void Start() {
        WinnerUI.SetActive(false);
        top5UI.SetActive(false);
        CurrentWaveLbl.text = "Wave " + WaveNumber + " of " + TotalWaves;
        PlayBtn.gameObject.SetActive(false);
        showMenu();
        top5Button.onClick.AddListener(() => {
            SceneManager.LoadScene("Leaderboard");
        });
    }

    void Update() {
        joystickStartOrPSPressed();
        updateTotalKilledLabels();
    }

    private void updateTotalKilledLabels() {
        playerOneTotalKilledLbl.text = PlayerOneTotalKilled.ToString();
        playerTwoTotalKilledLbl.text = PlayerTwoTotalKilled.ToString();
    }

    public override void setCurrentGameState() {
        if (WaveNumber == 0 && (TotalKilled + RoundEscaped) == 0) {
            CurrentState = gameStatus.play;
        } else if (WaveNumber >= TotalWaves) {
            CurrentState = gameStatus.win;
        } else {
            CurrentState = gameStatus.next;
        }
    }

    public override void showMenu() {
        switch (CurrentState) {
            case gameStatus.gameover:
                PlayBtnLbl.text = "Play Again!";
                /*FindObjectOfType<AudioManager>().StopPlayingSound("ThemeSong");
                FindObjectOfType<AudioManager>().PlaySound("LosingGameSound");
                if (firstTimeGameOver) {
                    GameOverUI.SetActive(true);
                    firstTimeGameOver = false;
                }*/
                PlayBtn.gameObject.SetActive(true);
                CurrentWaveLblObj.gameObject.SetActive(true);
                setStats();
                break;
            case gameStatus.next:
                PlayBtnLbl.text = "Next Wave";
                break;
            case gameStatus.play:
                PlayBtnLbl.text = "Play";
                break;
            case gameStatus.win:
                setStats();
                if (PlayerOneTotalKilled > PlayerTwoTotalKilled) {
                    winnerText.text = "Player 1";
                    WinnerScore.score = PlayerOneTotalKilled + 1;
                } else if (PlayerOneTotalKilled < PlayerTwoTotalKilled) {
                    winnerText.text = "Player 2";
                    WinnerScore.score = PlayerTwoTotalKilled + 1;
                } else {
                    winnerText.text = "Player 1 & Player 2";
                }
                leaderBoard leaderBoardScript = gameObject.GetComponent<leaderBoard>();
                if (leaderBoardScript.scores.Length > 0 && WinnerScore.score > leaderBoardScript.scores[4]) {
                    top5UI.SetActive(true);
                }
                WinnerUI.SetActive(true);
                break;
        }
        PlayBtn.gameObject.SetActive(true);
        CurrentWaveLblObj.gameObject.SetActive(true);

    }
}
