﻿using Assets.Scripts.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManagerPvpLevel2 : GameManager {
    #region SerializeFields
    [SerializeField]
    private Text playerOneTotalKilledLbl;
    [SerializeField]
    private Text playerTwoTotalKilledLbl;
    [SerializeField]
    private Text winnerText;
    [SerializeField]
    private GameObject WinnerUI;
    [SerializeField]
    private GameObject top5UI;
    [SerializeField]
    private Button top5Button;
    #endregion 

    #region Singleton
    private static GameManagerPvpLevel2 instance = null;

    public static GameManagerPvpLevel2 Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<GameManagerPvpLevel2>();
            } else if (instance != FindObjectOfType<GameManagerPvpLevel2>()) {
                Destroy(FindObjectOfType<GameManagerPvpLevel2>());
            }
            return instance;
        }
    }
    #endregion

    // Use this for initialization
    void Start() {
        WinnerUI.SetActive(false);
        top5UI.SetActive(false);
        CurrentWaveLbl.text = "Wave " + WaveNumber + " of " + TotalWaves;
        PlayBtn.gameObject.SetActive(false);
        showMenu();
        top5Button.onClick.AddListener(() => {
            SceneManager.LoadScene("Leaderboard");
        });
    }

    void Update() {
        joystickStartOrPSPressed();
        updateTotalKilledLabels();
    }

    private void updateTotalKilledLabels() {
        playerOneTotalKilledLbl.text = PlayerOneTotalKilled.ToString();
        playerTwoTotalKilledLbl.text = PlayerTwoTotalKilled.ToString();
    }

    public override IEnumerator spawn() {
        if (EnemiesPerSpawn > 0 && EnemyList.Count < TotalEnemies) {
            for (int i = 0; i < EnemiesPerSpawn; i++) {
                if (EnemyList.Count < TotalEnemies) {
                    int index;
                    Enemy newEnemy;
                    System.Random rnd = new System.Random();
                    if (i % 2 == 0) {
                        index = 0;
                        newEnemy = Instantiate(Minions[rnd.Next(0, 2)]) as Enemy;
                    } else {
                        index = 1;
                        newEnemy = Instantiate(Minions[rnd.Next(2, 4)]) as Enemy;
                    }
                    newEnemy.StartIndex = 0;
                    newEnemy.transform.position = SpawnPoints[index].transform.position;
                    newEnemy.GameManager = this;
                    yield return new WaitForSeconds(0.5f);
                }
            }
            yield return new WaitForSeconds(1f);
            StartCoroutine(spawn());
        }
    }

    public override void setCurrentGameState() {
        if (WaveNumber == 0 && (TotalKilled + RoundEscaped) == 0) {
            CurrentState = gameStatus.play;
        } else if (WaveNumber >= TotalWaves) {
            CurrentState = gameStatus.win;
        } else {
            CurrentState = gameStatus.next;
        }
    }

    public override void showMenu() {
        switch (CurrentState) {
            case gameStatus.gameover:
                PlayBtnLbl.text = "Play Again!";
                PlayBtn.gameObject.SetActive(true);
                CurrentWaveLblObj.gameObject.SetActive(true);
                setStats();
                break;
            case gameStatus.next:
                PlayBtnLbl.text = "Next Wave";
                break;
            case gameStatus.play:
                PlayBtnLbl.text = "Play";
                break;
            case gameStatus.win:
                setStats();
                if (PlayerOneTotalKilled > PlayerTwoTotalKilled) {
                    winnerText.text = "Player 1";
                    WinnerScore.score = PlayerOneTotalKilled + 1;
                } else if (PlayerOneTotalKilled < PlayerTwoTotalKilled) {
                    winnerText.text = "Player 2";
                    WinnerScore.score = PlayerTwoTotalKilled + 1;
                } else {
                    winnerText.text = "Player 1 & Player 2";
                }
                leaderBoard leaderBoardScript = gameObject.GetComponent<leaderBoard>();
                if (leaderBoardScript.scores.Length > 0 && WinnerScore.score > leaderBoardScript.scores[4]) {
                    top5UI.SetActive(true);
                }
                WinnerUI.SetActive(true);
                break;
        }
        PlayBtn.gameObject.SetActive(true);
        CurrentWaveLblObj.gameObject.SetActive(true);
    }

}
