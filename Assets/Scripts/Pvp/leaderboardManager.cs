﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class leaderboardManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        joystickButtons();
    }

    private void joystickButtons() {
        if (Input.GetButtonDown("1PS4_O") || Input.GetButtonDown("2PS4_O")) {
            MainMenu();
        }
    }

    public void MainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
