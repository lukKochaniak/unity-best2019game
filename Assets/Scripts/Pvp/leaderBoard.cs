﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PubNubAPI;
using UnityEngine.UI;
using SimpleJSON;
using TMPro;

public class MyClass {
    public string username;
    public int score;
    public string test;
}
public class leaderBoard : MonoBehaviour {
    public static PubNub pubnub;
    public TextMeshProUGUI Line1 = null;
    public TextMeshProUGUI Line2 = null;
    public TextMeshProUGUI Line3 = null;
    public TextMeshProUGUI Line4 = null;
    public TextMeshProUGUI Line5 = null;
    public TextMeshProUGUI Score1 = null;
    public TextMeshProUGUI Score2 = null;
    public TextMeshProUGUI Score3 = null;
    public TextMeshProUGUI Score4 = null;
    public TextMeshProUGUI Score5 = null;
    public Button SubmitButton = null;
    public InputField FieldUsername = null;
    // Use this for initialization

    public string[] players;
    public int[] scores;
    public int winnerScore = 0;
    void Start() {
        if (SubmitButton != null) {
            Button btn = SubmitButton.GetComponent<Button>();
            btn.onClick.AddListener(TaskOnClick);
        }
        winnerScore = WinnerScore.score;
        WinnerScore.score = 0;
        if (SubmitButton && winnerScore == 0) {
            SubmitButton.gameObject.SetActive(false);
            FieldUsername.gameObject.SetActive(false);
        }
            // Use this for initialization
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.PublishKey = "pub-c-fcc18146-c8eb-49fc-9d46-c62b6b2c8469";
        pnConfiguration.SubscribeKey = "sub-c-06586d60-f70b-11e8-a591-5ae93f5cd42c";
        pnConfiguration.LogVerbosity = PNLogVerbosity.BODY;
        pnConfiguration.UUID = Random.Range(0f, 999999f).ToString();
        pubnub = new PubNub(pnConfiguration);
        Debug.Log(pnConfiguration.UUID);

        MyClass myFireObject = new MyClass();
        myFireObject.test = "new user";
        string fireobject = JsonUtility.ToJson(myFireObject);
        pubnub.Fire()
          .Channel("my_channel")
          .Message(fireobject)
          .Async((result, status) => {
              if (status.Error) {
                  Debug.Log(status.Error);
                  Debug.Log(status.ErrorData.Info);
              } else {
                  Debug.Log(string.Format("Fire Timetoken: {0}", result.Timetoken));
              }
          });

        pubnub.SusbcribeCallback += (sender, e) => {
            SusbcribeEventEventArgs mea = e as SusbcribeEventEventArgs;
            if (mea.Status != null) {
            }
            if (mea.MessageResult != null) {
                Dictionary<string, object> msg = mea.MessageResult.Payload as Dictionary<string, object>;
                players = msg["username"] as string[];
                scores = msg["score"] as int[];
                int usernamevar = 1;
                Debug.Log("Last Score: " + scores[4]);
                if (Line1 != null) { 
                    Line1.text = "1. " + players[0];
                    Line2.text = "2. " + players[1];
                    Line3.text = "3. " + players[2];
                    Line4.text = "4. " + players[3];
                    Line5.text = "5. " + players[4];
                    Score1.text = scores[0].ToString();
                    Score2.text = scores[1].ToString();
                    Score3.text = scores[2].ToString();
                    Score4.text = scores[3].ToString();
                    Score5.text = scores[4].ToString();
                }
            }
            if (mea.PresenceEventResult != null) {
                Debug.Log("In Example, SusbcribeCallback in presence" + mea.PresenceEventResult.Channel + mea.PresenceEventResult.Occupancy + mea.PresenceEventResult.Event);
            }
        };
        pubnub.Subscribe()
          .Channels(new List<string>() {
        "my_channel2"
          })
          .WithPresence()
          .Execute();
    }
    void TaskOnClick() {
        if (FieldUsername != null) { 
            var usernametext = FieldUsername.text;// this would be set somewhere else in the code
            var scoretext = winnerScore;
            MyClass myObject = new MyClass();
            myObject.username = FieldUsername.text;
            myObject.score = winnerScore;
            string json = JsonUtility.ToJson(myObject);
            pubnub.Publish()
              .Channel("my_channel")
              .Message(json)
              .Async((result, status) => {
                  if (!status.Error) {
                      Debug.Log(string.Format("Publish Timetoken: {0}", result.Timetoken));
                  } else {
                      Debug.Log(status.Error);
                      Debug.Log(status.ErrorData.Info);
                  }
              });
            //Output this to console when the Button is clicked
            Debug.Log("You have clicked the button!");
            SubmitButton.gameObject.SetActive(false);
            FieldUsername.gameObject.SetActive(false);
        }
    }

    public void savePlayerInTop5(string playerName, int playerScore) {
        MyClass myObject = new MyClass();
        myObject.username = playerName;
        myObject.score = playerScore;
        //myObject.username = FieldUsername.text;
        //myObject.score = FieldScore.text;
        string json = JsonUtility.ToJson(myObject);
        pubnub.Publish()
          .Channel("my_channel")
          .Message(json)
          .Async((result, status) => {
              if (!status.Error) {
                  Debug.Log(string.Format("Publish Timetoken: {0}", result.Timetoken));
              } else {
                  Debug.Log(status.Error);
                  Debug.Log(status.ErrorData.Info);
              }
          });
        //Output this to console when the Button is clicked
        Debug.Log("You have clicked the button!");
    }
};