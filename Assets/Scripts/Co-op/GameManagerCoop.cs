﻿using Assets.Scripts.Common;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerCoop : GameManager {
    #region SerializeFields
    private float health;
    [SerializeField]
    private float maxHealth;
    [SerializeField]
    private Transform healthBar;
    [SerializeField]
    private Slider healthFill;
    #endregion

    #region GettersAndSetter
    public float Health {

        get { return health; }
        set { health = value; }
    }
    #endregion

    #region Singleton
    private static GameManagerCoop instance = null;

    public static GameManagerCoop Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<GameManagerCoop>();
            } else if (instance != FindObjectOfType<GameManagerCoop>()) {
                Destroy(FindObjectOfType<GameManagerCoop>());
            }

            //DontDestroyOnLoad(FindObjectOfType<T>());
            return instance;
        }
    }
    #endregion

    void Start() {
        PlayBtn.gameObject.SetActive(false);
        health = maxHealth;
        CurrentWaveLbl.text = "Wave " + WaveNumber + " of " + TotalWaves;
        showMenu();
    }

    void Update() {
        PositionHealthBar();
        GameOverIfNoRemainingHp();
        joystickStartOrPSPressed();
    }

    private void PositionHealthBar() {
        healthFill.value = health / maxHealth;
        //cooldownBar.LookAt(Camera.main.transform);
    }

    public void GameOverIfNoRemainingHp() {
        if (health <= 0) {
            CurrentState = gameStatus.gameover;
            DestroyAllEnemies();
            showMenu();
        }
    }
                                
    public override void setCurrentGameState() {
        if (health <= 0) {
            CurrentState = gameStatus.gameover;
            DestroyAllEnemies();
            showMenu();
        } else if (WaveNumber == 0 && (TotalKilled + RoundEscaped) == 0) {
            CurrentState = gameStatus.play;
        } else if (WaveNumber >= TotalWaves) {
            CurrentState = gameStatus.next;
        } else {
            CurrentState = gameStatus.next;
        }
    }
}
